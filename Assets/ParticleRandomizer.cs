﻿using UnityEngine;
using System.Collections;

public class ParticleRandomizer : MonoBehaviour {

    public Material[] m_materialList;
	
	// Update is called once per frame
	void Update()
    {
        if (GetComponent<ParticleSystem>().isPlaying)
        {
            ParticleSystemRenderer rend = (ParticleSystemRenderer)GetComponent<ParticleSystem>().GetComponent<ParticleSystemRenderer>();
            int randomNumber = Random.Range(0, m_materialList.Length);
            rend.material = m_materialList[randomNumber];
        }
	}
}
