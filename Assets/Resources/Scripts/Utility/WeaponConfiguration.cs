﻿using UnityEngine;
using System.Collections;

public class WeaponConfiguration {

    public string weaponName;
    public int weaponCost;
    public int weaponBaseCost;
    public WeaponBase.WEAPON_TYPE weaponType;
    public WeaponBase.Attributes weaponAttributes;
    public bool weaponEquipped;
    public bool weaponSellable;

    public void Initialize(string _weaponName = "NONAME", WeaponBase.WEAPON_TYPE _weaponType = WeaponBase.WEAPON_TYPE.NONE, WeaponBase.Attributes _weaponAttributes = null, int _weaponBaseCost = 0, bool _weaponSellable = true)
    {
        weaponName = _weaponName;
        weaponType = _weaponType;
        weaponSellable = _weaponSellable;

        if(_weaponAttributes == null)
        {
            weaponAttributes = new WeaponBase.Attributes(0,0);
        }
        else
        {
            weaponAttributes = _weaponAttributes;
        }

        if (_weaponBaseCost == 0)
        {
            weaponBaseCost = (int)_weaponType * 200; // Multiplier hard code. Sick ;p
        }
        else
        {
            weaponBaseCost = _weaponBaseCost;
        }

        weaponCost = weaponBaseCost;
    }

    public void CalculateValue()
    {
        weaponCost = weaponBaseCost;
        float totalPoints = (float)weaponAttributes.m_damage + (float)weaponAttributes.m_shield;
        
        for (int i = 1; i <= totalPoints; i++)
        {
            weaponCost += (int)((int)weaponBaseCost * ((float)i / 20.0f));
        }
    }
}
