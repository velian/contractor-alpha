﻿using UnityEngine;
using System.Collections;

public class Utility : MonoBehaviour {

	public static GameObject FindInactive(string _name)
    {
        GameObject[] list = (GameObject[])Resources.FindObjectsOfTypeAll(typeof(GameObject));

        foreach(GameObject obj in list)
        {
            if (obj.name == _name)
                return obj;
        }

        return null;
    }
}
