﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class DialogueUI : MonoBehaviour {

    [System.Serializable]
    public struct DialogueSet
    {
        public DialogueSet(List<string> _dialogue)
        {
            m_dialogue = _dialogue;
            m_color = Color.white;
            m_relaySpeed = 0.1f;
        }

        public List<string> m_dialogue;
        public Color m_color;
        public float m_relaySpeed;
    }

    public bool m_showDialogue = false;
    public bool m_open = false;
    public float m_lerpDuration = 1.0f;

    [Header("GameObjects Of UI Elements")]
    public GameObject m_background;
    public GameObject m_lowerPanel;
    public GameObject m_leftPortrait;
    public GameObject m_rightPortrait;

    [Header("Starting Position Transforms")]
    public RectTransform m_lowerPanelTransform;
    public RectTransform m_leftPortraitTransform;
    public RectTransform m_rightPortraitTransform;

    DialogueSet? m_currentSet;
    int m_setIndex = 0;
    bool m_characterScramble = false;
    bool m_updatedText = false;

    Vector3 m_lowerPanelTransformOrigin;
    Vector3 m_leftPortraitTransformOrigin;
    Vector3 m_rightPortraitTransformOrigin;

    Sprite m_blackPixel;

	// Use this for initialization
	void Start ()
    {
        m_lowerPanelTransformOrigin = m_lowerPanel.GetComponent<RectTransform>().position;
        m_leftPortraitTransformOrigin = m_leftPortrait.GetComponent<RectTransform>().position;
        m_rightPortraitTransformOrigin = m_rightPortrait.GetComponent<RectTransform>().position;

        m_background.GetComponent<Image>().DOFade(0, 0);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (m_showDialogue && !m_open) //If show dialogue and not shown
        {
            m_background.GetComponent<Image>().DOFade(0.8f, m_lerpDuration).OnComplete(()=>
            {
                m_open = true;                
            });

            GameObject.Find("Player").GetComponent<PlayerController>().m_lockControls = true;

            m_lowerPanel.GetComponent<RectTransform>().DOMove(m_lowerPanelTransform.position, m_lerpDuration);
            m_leftPortrait.GetComponent<RectTransform>().DOMove(m_leftPortraitTransform.position, m_lerpDuration);
            m_rightPortrait.GetComponent<RectTransform>().DOMove(m_rightPortraitTransform.position, m_lerpDuration);
        }
        else if (!m_showDialogue && m_open) //else hide dialogue if shown
        {
            m_open = false;

            GameObject.Find("Player").GetComponent<PlayerController>().m_lockControls = false;

            m_background.GetComponent<Image>().DOFade(0, m_lerpDuration);
            m_lowerPanel.GetComponent<RectTransform>().DOMove(m_lowerPanelTransformOrigin, m_lerpDuration);
            m_leftPortrait.GetComponent<RectTransform>().DOMove(m_leftPortraitTransformOrigin, m_lerpDuration);
            m_rightPortrait.GetComponent<RectTransform>().DOMove(m_rightPortraitTransformOrigin, m_lerpDuration);
        }
        if(m_currentSet != null && m_showDialogue && m_open)
        {
            RenderSet();
        }
	}

    public void Show()
    {
        if (!m_showDialogue)
            m_showDialogue = true;
    }

    public void PerformSequence(DialogueSet _dialogueSet)
    {
        m_currentSet = _dialogueSet;
    }

    void RenderSet()
    {
        if (!m_characterScramble && m_lowerPanel.GetComponentInChildren<Text>().text != m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex])
        {
            if(!m_updatedText)
            {
                if (m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex].Contains("%LEFT%"))
                {
                    m_leftPortrait.GetComponent<Image>().DOFade(1f, 0.5f);
                    m_rightPortrait.GetComponent<Image>().DOFade(0.4f, 0.5f);
                    m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex] = m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex].Replace("%LEFT%", "");
                }
                else if (m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex].Contains("%RIGHT%"))
                {
                    m_rightPortrait.GetComponent<Image>().DOFade(1f, 0.5f);
                    m_leftPortrait.GetComponent<Image>().DOFade(0.4f, 0.5f);
                    m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex] = m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex].Replace("%RIGHT%", "");
                }
                else
                {
                    m_rightPortrait.GetComponent<Image>().DOFade(0.4f, 0.5f);
                    m_leftPortrait.GetComponent<Image>().DOFade(0.4f, 0.5f);
                }
                m_updatedText = true;
            }

            m_lowerPanel.GetComponentInChildren<Text>().DOText(m_currentSet.GetValueOrDefault().m_dialogue[m_setIndex], m_currentSet.GetValueOrDefault().m_relaySpeed, true, ScrambleMode.All)
            .OnStart(() =>
            {
                m_characterScramble = true;
            })
            .OnComplete(() =>
            {
                m_characterScramble = false;
            });
        }

        if (GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.A, GamepadInput.GamePad.Index.One) || Input.GetKeyDown(KeyCode.Space))
        {
            m_updatedText = false;

            if (m_currentSet.GetValueOrDefault().m_dialogue.Count - 1 == m_setIndex)
            {
                m_currentSet = null;
                m_showDialogue = false;
                m_setIndex = 0;
                m_lowerPanel.GetComponentInChildren<Text>().text = "";
                return;
            }
            
            m_setIndex++;
        }
    }
}
