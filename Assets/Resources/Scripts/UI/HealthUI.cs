﻿using UnityEngine;
using System.Collections;

public class HealthUI : MonoBehaviour {

    public float? m_currentHealth;
    public float? m_totalHealth;

    public float m_healthRegenTime = 0.5f;

    public int m_currentThreshold;
    public int m_childIndex;

    public int m_childCount = 5;

    RectTransform[] m_children; // Will always have a size of 5

    bool m_initialized = false;
    bool m_regenerating = false;


	// Use this for initialization
	void Start()
    {
        
	}

    void Initialize()
    {
        //m_totalHealth = m_shieldTotal;
        m_currentHealth = m_totalHealth;

        m_children = new RectTransform[m_childCount];

        for (int i = 0; i < m_childCount; i++)
        {
            m_children[i] = transform.FindChild("Chunk" + (i + 1)).GetComponent<RectTransform>();
        }

        m_childIndex = m_childCount - 1;
        m_currentThreshold = (int)m_totalHealth / m_childCount;

        m_initialized = true;
    }
	
	// Update is called once per frame
	void Update()
    {
        //Init if we haven't already
	    if(!m_initialized)
        {           
            Initialize();
            return;
        }
       
        //Check if we need to regenerate health
        if(m_currentHealth != m_totalHealth)
        {
            //Begin health regen coroutine if we haven't already
            if(!m_regenerating)
            {
                StartCoroutine(IHealthRegen());
            }
        }

        ///DEBUG KEY BINDS///
        if(Input.GetKeyDown(KeyCode.Q))
        {
            RemoveHealth(1);
        }

	}

    public void RemoveHealth(float _damage)
    {
        if (m_childIndex < 0) return;

        //Remove damage from current health
        m_currentHealth -= _damage;

        //Stop regen coroutine - forced
        StopAllCoroutines();
        m_regenerating = false;

        //remove health block if we need to
        if(m_currentHealth <= m_currentThreshold)
        {
            //Remove block and decrement child
            m_children[m_childIndex--].gameObject.SetActive(false);

            //Set new threshhold
            m_currentThreshold = (m_childIndex == 0)? 0 : (int)m_totalHealth / m_childIndex;
        }
    }

    IEnumerator IHealthRegen()
    {
        m_regenerating = true;
        yield return new WaitForSeconds(m_healthRegenTime);

        //Add all health blocks
        m_currentHealth = m_totalHealth;

        if (m_childIndex != m_childCount - 1) m_childIndex = m_childCount - 1;
        
        foreach(RectTransform child in m_children)
        {
            child.gameObject.SetActive(true);
        }

        m_regenerating = false;
    }
}
