﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CarbineUI : MonoBehaviour
{
    public GameObject m_carbineObject;
    public Text m_ammunitionUI;
    public Sprite m_bulletIcon;
    public float m_iconScale;

    void Start()
    {
        //gameObject.SetActive(false);
    }

    void Update()
    {
        if(m_carbineObject == null)
        {
            m_carbineObject = Utility.FindInactive("Carbine");
            gameObject.SetActive(false);
        }

        //Set ammunition text to weapon current ammo number
        m_ammunitionUI.text = m_carbineObject.GetComponent<WeaponBase>().m_ammunition.ToString();
    }

    public void AddBullet(int _index)
    {
        Transform bulletPos = transform.FindChild("Carbine Manager").FindChild("Chambers").FindChild("BulletAddPosition (" + _index + ")").transform;

        GameObject bullet = new GameObject("Bullet" + _index);
        //bullet.transform.position = bulletPos.position;
        bullet.AddComponent<CanvasRenderer>();
        bullet.AddComponent<Image>();
        bullet.GetComponent<Image>().sprite = m_bulletIcon;
        bullet.transform.SetParent(bulletPos);
        bullet.transform.position = bulletPos.position;

        bullet.GetComponent<Image>().transform.localScale = new Vector3(m_iconScale, m_iconScale, m_iconScale);
        //Instantiate(bullet);
    }

    public void RemoveBullet(int _index)
    {
        if (GameObject.Find("Bullet" + _index))
        {
            //m_shotgunObject.GetComponent<Shotgun>().m_currentRound--;
            Destroy(GameObject.Find("Bullet" + _index));
        }
    }

    public void UnCockRound()
    {
        GameObject bullet = GameObject.Find("Bullet0");

        if (bullet != null)
        {
            bullet.GetComponent<Image>().color = Color.red;
        }
    }

    public void CockRound()
    {
        GameObject bullet = GameObject.Find("Bullet0");

        if(bullet != null)
        {
            bullet.GetComponent<Image>().color = Color.green;
        }
    }
}
