﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WorldSpaceUI : MonoBehaviour {

    public Transform m_objectTrans;
    float m_offset = 1f;

	public void Initialize(Transform _trans, Vector2 _scale)
    {
        m_objectTrans = _trans;
        transform.localScale = _scale;        
    }

	// Update is called once per frame
	void Update()
    {
        transform.position = new Vector3(m_objectTrans.position.x, m_offset + Mathf.Sin(Time.time) * 0.25f, m_objectTrans.position.z);    
	}
}
