﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class RevolverUI : MonoBehaviour {

    public GameObject m_revolverObject;
    public Sprite m_bulletIcon;
    public Text m_ammunitionUI;
    public float m_iconScale;
    public bool m_rotating = false;
    public float m_rotationDegrees;
    public float m_speed;
    public Ease m_rotateEase;
    //bool initialized = false;

    void Start()
    {

    }

    void Update()
    {
        if(m_revolverObject == null)
        {
            m_revolverObject = Utility.FindInactive("Revolver");
            gameObject.SetActive(true);
        }

        //Set ammunition text to weapon current ammo number
        m_ammunitionUI.text = m_revolverObject.GetComponent<WeaponBase>().m_ammunition.ToString();
    }

    public void AddBullet(int _index)
    {
        Transform bulletPos = transform.FindChild("Cylinder Manager").FindChild("Cylinder").FindChild("BulletAddPosition (" + _index + ")").transform;

        GameObject bullet = new GameObject("Bullet" + _index);
        //bullet.transform.position = bulletPos.position;
        bullet.AddComponent<CanvasRenderer>();
        bullet.AddComponent<Image>();
        bullet.GetComponent<Image>().sprite = m_bulletIcon;
        bullet.transform.SetParent(GameObject.FindGameObjectWithTag("RevolverCylinder").transform);
        bullet.transform.position = bulletPos.position;

        bullet.GetComponent<Image>().transform.localScale = new Vector3(m_iconScale, m_iconScale, m_iconScale);
        //Instantiate(bullet);
    }

    public void RemoveBullet(int _index)
    {
        if(GameObject.Find("Bullet" + _index))
        {
            Rotate();
            m_revolverObject.GetComponent<Revolver>().NextCylinder();
            Destroy(GameObject.Find("Bullet" + _index));
        }        
    }

    public void Rotate(float _degrees = 0f)
    {
        //if(!m_rotating)
        //{
        //if (_degrees != 0f)
        //{
        //    transform.FindChild("Cylinder Manager").transform
        //        .DORotate(new Vector3(0, 0, (int)_degrees * -360), m_speed, RotateMode.WorldAxisAdd)
        //        .OnStart(() => { m_rotating = true; })
        //        .OnComplete(() => { m_rotating = false; })
        //        .SetEase(m_rotateEase);
        //}
        //else
        //{
            transform.FindChild("Cylinder Manager").transform
                .DOBlendableRotateBy(new Vector3(0, 0, m_rotationDegrees), m_speed, RotateMode.WorldAxisAdd)
                .OnStart(() => { m_rotating = true; })
                .OnComplete(() => { m_rotating = false; })
                .SetEase(m_rotateEase);
        //}
        //}
    }

    public bool GetRotating()
    {
        return m_rotating;
    }
}
