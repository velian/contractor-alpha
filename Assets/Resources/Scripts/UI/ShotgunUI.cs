﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShotgunUI: MonoBehaviour
{

    public GameObject m_shotgunObject;
    public Text m_ammunitionUI;
    public Sprite m_bulletIcon;
    public float m_iconScale;

    void Start()
    {
        //gameObject.SetActive(false);
    }

    void Update()
    {
        if (m_shotgunObject == null)
        {
            m_shotgunObject = Utility.FindInactive("Shotgun");
            gameObject.SetActive(false);
        }

        m_ammunitionUI.text = m_shotgunObject.GetComponent<WeaponBase>().m_ammunition.ToString();
    }

    public void AddBullet(int _index)
    {
        Transform bulletPos = transform.FindChild("Shotgun Manager").FindChild("Chambers").FindChild("BulletAddPosition (" + _index + ")").transform;

        GameObject bullet = new GameObject("Bullet" + _index);
        //bullet.transform.position = bulletPos.position;
        bullet.AddComponent<CanvasRenderer>();
        bullet.AddComponent<Image>();
        bullet.GetComponent<Image>().sprite = m_bulletIcon;
        bullet.transform.SetParent(bulletPos);
        bullet.transform.position = bulletPos.position;

        bullet.GetComponent<Image>().transform.localScale = new Vector3(m_iconScale, m_iconScale, m_iconScale);
        //Instantiate(bullet);
    }

    public void RemoveBullet(int _index)
    {
        if (GameObject.Find("Bullet" + _index))
        {
            //m_shotgunObject.GetComponent<Shotgun>().m_currentRound--;
            Destroy(GameObject.Find("Bullet" + _index));
        }
    }
}
