﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BulletHole : MonoBehaviour {

    public uint m_timerMax = 5;

	// Use this for initialization
	void Start()
    {
        //Simple material fade, deletes object on complete
        GetComponent<Renderer>().material.DOFade(0f, m_timerMax).OnComplete(()=>{Destroy(this.gameObject);});
	}
}
