﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WeaponBase : MonoBehaviour
{
    [System.Serializable]
    public class Attributes
    {
        public uint m_damage = 1;
        public uint m_shield = 1;

        public Attributes(int _damage, int _shield)
        {
            m_damage = (uint)_damage;
            m_shield = (uint)_shield;
        }
    }

    public enum WEAPON_TYPE
    {
        NONE,
        PISTOL,
        CARBINE,
        SHOTGUN
    }

    //Variables
    public GameObject m_model = null;
    public GameObject m_bulletPrefab = null;
    //public GameObject m_laserDot = null;

    public GameObject m_target = null;
    public GameObject m_ui = null;

    public AudioClip m_gunShot;
    public AudioClip m_gunEmpty;
    public AudioClip m_gunReload;

    public Attributes m_attributes = new Attributes(0,0);

    public bool m_playerWeapon = false;
    public bool m_infiniteAmmo = false;

    public int m_weaponBaseValue = 100; //price of weapon without upgrades
    public int m_weaponLockDistance = 25;

    public bool m_sellable = true;
    public bool m_equipped = false;

    public int m_clipSize = 0;
    public int m_clip = 0;
    public int m_ammunition = 0;

    public int m_numberOfBullets = 1;

    public Color m_bulletColor = Color.cyan;    

    public float m_spreadMax = 0.1f;

    public float m_fireCooldown = 0.2f;
    public float m_reloadCooldown = 0.1f;

    public string m_weaponName = null;

    public WEAPON_TYPE m_weaponType = WEAPON_TYPE.NONE;

    public int m_weaponCurrentValue = 0;

    protected bool m_reloading = false;
    protected bool m_firing = false;

	// Use this for initialization
	void Start ()
    {
        if (m_model == null)
        {
            print("Tried to initialzed weapon without model.");
        }

        if (m_bulletPrefab == null)
        {
            print("Tried to initialzed weapon without bullet prefab.");
        }

        if (m_weaponName == null)
        {
            print("Tried to initialize weapon with no name.");
        }
	}
    
    public void Initialize(WeaponBase _weaponReference)
    {
        m_bulletPrefab = _weaponReference.m_bulletPrefab;
        m_gunShot = _weaponReference.m_gunShot;
        m_gunEmpty = _weaponReference.m_gunEmpty;
        m_gunReload = _weaponReference.m_gunReload;
        m_clipSize = _weaponReference.m_clipSize;
        m_ammunition = _weaponReference.m_ammunition;
        m_spreadMax = _weaponReference.m_spreadMax;

        m_bulletColor = _weaponReference.m_bulletColor;
        m_fireCooldown = _weaponReference.m_fireCooldown;
        m_reloadCooldown = _weaponReference.m_reloadCooldown;
        m_numberOfBullets = _weaponReference.m_numberOfBullets;
        m_attributes = _weaponReference.m_attributes;
        //m_laserDot = Instantiate<GameObject>(_weaponReference.m_laserDot);

        m_playerWeapon = true;
        m_infiniteAmmo = true;

        switch (m_weaponType)
        {
            case WEAPON_TYPE.NONE:
                break;
            case WEAPON_TYPE.PISTOL:
            {
                m_ui = GameObject.Find("UserInterface").transform.FindChild("RevolverUI").gameObject;
                m_ui.GetComponent<RevolverUI>().m_revolverObject = this.gameObject;
            }break;
            case WEAPON_TYPE.CARBINE:
            {
                m_ui = GameObject.Find("UserInterface").transform.FindChild("CarbineUI").gameObject;
                m_ui.GetComponent<CarbineUI>().m_carbineObject = this.gameObject;            
            } break;
            case WEAPON_TYPE.SHOTGUN:
            {
                m_ui = GameObject.Find("UserInterface").transform.FindChild("ShotgunUI").gameObject;
                m_ui.GetComponent<ShotgunUI>().m_shotgunObject = this.gameObject;            
            } break;
        }


        gameObject.AddComponent<LineRenderer>();
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = (Material)Resources.Load<Material>("Materials/blasterbolt");
        lineRenderer.SetColors(Color.red, Color.clear);
        lineRenderer.SetPosition(0, Vector3.zero);
        lineRenderer.SetPosition(1, Vector3.zero);
        lineRenderer.SetWidth(0.02f, 0.02f);
    }

    /// <summary>
    /// Calls weapon update
    /// </summary>
    void Update()
    {
        if (m_playerWeapon && !GameObject.Find("Player").GetComponent<PlayerController>().m_lockControls)
        {
            WeaponUpdate();

            //Render Laser Sights

            if (GetTarget() != null)
            {
                GetComponent<LineRenderer>().SetPosition(0, transform.position);
                GetComponent<LineRenderer>().SetPosition(1, GetTarget());
            }
            else
            {
                GetComponent<LineRenderer>().SetPosition(1, Vector3.zero);
                GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
            }
        }        
    }

    public void ChildToPlayer()
    {
        transform.parent = GameObject.Find("WeaponTransform").transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

	// Update is called once per frame
	public virtual void WeaponUpdate()
    {
        
	}

    /// <summary>
    /// Instantiates a bullet for each number of bullets
    /// </summary>
    public void Fire()
    {
        if (m_firing || m_reloading) return;

        AudioSource.PlayClipAtPoint(m_gunShot, transform.position, 0.2f);

        Camera.main.DOShakePosition(0.2f, 1, 15);

        StartCoroutine(FireEnumerator());
    }

    public Vector3 GetTarget()
    {
        //if(Vector3.Angle(-transform.right, transform.forward) <= m_weaponFOV / 2)
        //{

        if (!m_playerWeapon)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, -transform.right, out hitInfo))
            {
                return hitInfo.point;
            }
        }
        else
        {
            float nearest = Mathf.Infinity;
            GameObject enemy = null;
            Ray thisRay = new Ray(transform.position, -transform.right);
            foreach(var hit in Physics.SphereCastAll(thisRay, 1f, m_weaponLockDistance))
            {
                if(hit.transform.CompareTag("Enemy"))
                {
                    Vector3 hitDirection = hit.transform.position - transform.position;

                    RaycastHit hitInfo;
                    if (Physics.Raycast(transform.position, hitDirection, out hitInfo, m_weaponLockDistance))
                    {
                        if (hitInfo.transform.CompareTag("Enemy"))
                        {
                            var enemyDirection = hit.transform.position - transform.position;
                            float angle = Quaternion.FromToRotation(transform.forward, enemyDirection).eulerAngles.y;

                            if (angle < nearest)
                            {
                                enemy = hit.transform.gameObject;
                                nearest = angle;
                            } 
                        }
                    }                    
                }
            }

            if (enemy != null)
            {
                m_target = enemy;
                return m_target.transform.position;
            }
            else
            {
                m_target = enemy;

                RaycastHit hitInfo;
                if (Physics.Raycast(transform.position, -transform.right, out hitInfo, m_weaponLockDistance))
                {
                    m_target = null;
                    return hitInfo.point;
                }
            }
        }

        return transform.position;
    }

    IEnumerator FireEnumerator()
    {
        m_firing = true;

        AudioSource.PlayClipAtPoint(m_gunShot, transform.position);

        //if (m_playerWeapon)
            //GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().RemoveBullet();

        for (int i = 0; i < m_numberOfBullets; i++)
        {
            Vector3 localForward = -transform.right;

            GameObject bullet;
            bullet = (GameObject)Instantiate((GameObject)m_bulletPrefab, transform.position + (localForward * (0.25f)), new Quaternion(1, 1, 1, 1));

            Vector3 bulletForward = (m_target == null)? localForward : m_target.transform.position - transform.position;

            bullet.GetComponent<Bullet>().m_direction = bulletForward + new Vector3(Random.Range(-m_spreadMax, m_spreadMax), Random.Range(-m_spreadMax, m_spreadMax), Random.Range(-m_spreadMax, m_spreadMax));
            bullet.transform.LookAt(new Vector3(transform.position.x, 0, transform.position.z));
            //bullet.GetComponent<Bullet>().m_holePrefab = m_holePrefab;

            if(m_playerWeapon)
                bullet.GetComponent<Bullet>().m_owner = GetComponentInParent<Transform>().GetComponentInParent<Transform>().tag;
            else
                bullet.GetComponent<Bullet>().m_owner = GetComponentInParent<Transform>().tag;

            bullet.GetComponent<Bullet>().m_damage = m_attributes.m_damage;
            bullet.GetComponent<Renderer>().material.color = m_bulletColor;
        }

        m_firing = false;

        yield return new WaitForSeconds(m_fireCooldown);        
    }

    /// <summary>
    /// Initializes the ReloadEnumerator coroutine.
    /// </summary>
    public void Reload()
    {
        //If we're reloading already, don't need to run any reload stuff
        if (m_reloading) return;

        AudioSource.PlayClipAtPoint(m_gunReload, transform.position);

        StartCoroutine(ReloadEnumerator());
    }

    /// <summary>
    /// Reload enumerator; called upon reload to initialze a coroutine that ticks for the reload cooldown
    /// </summary>
    /// <returns>Wait for seconds</returns>
    IEnumerator ReloadEnumerator()
    {
        m_reloading = true;

        yield return new WaitForSeconds(m_reloadCooldown);

        if (m_clip != m_clipSize && m_ammunition != 0 || m_infiniteAmmo)
        {
            m_clip++;
            m_ammunition = (m_infiniteAmmo) ? 0 : m_ammunition - 1;
        }

        m_reloading = false;
    }

    /// <summary>
    /// Calculates the value of this weapon
    /// </summary>
    //public int CalculateCurrentValue()
    //{
    //    float totalPoints = (float)m_attributes.m_damage + (float)m_attributes.m_shield;
    //    float currVal = 0;
    //
    //    for (int i = 1; i <= totalPoints; i++) //Why less than - equal to? Why is i = 1?
    //    {
    //        currVal += ((float)m_weaponBaseValue * ((float)i / 20.0f)); // Loving the magic number here.
    //    }
    //
    //    //print(currVal);
    //    m_weaponCurrentValue = (int)currVal;
    //    return m_weaponCurrentValue; 
    //}
}
