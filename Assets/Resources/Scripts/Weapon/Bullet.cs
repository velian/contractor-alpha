﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Bullet : MonoBehaviour {

    public Vector3 m_direction;
    public float m_speed;
    public float m_timer = 3;
    public string m_owner;
    public float m_damage;
    public GameObject m_holePrefab;

    float m_originDistance;

    Vector3 m_origin;

	// Use this for initialization
	void Start()
    {
        //Raycast to location and lerp over speed
        m_origin = transform.position;

        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position, m_direction, out hitInfo))
        {
            m_originDistance = Vector3.Distance(m_origin, hitInfo.transform.position);
            Debug.Log(hitInfo.transform.name);
            StartCoroutine(LinearInterpolate(hitInfo));
        }
        else
            Destroy(this.gameObject);
	}

    IEnumerator LinearInterpolate(RaycastHit point)
    {
        transform.DOMove(point.point, m_speed * m_originDistance * Time.deltaTime )
        .OnComplete(() =>
        {
            Quaternion rot = Quaternion.FromToRotation(-Vector3.forward, point.normal);
            GameObject bulletHole = (GameObject)Instantiate(m_holePrefab, point.point, rot);

            //Check the point's tag
            if (point.transform.tag == "Enemy" && m_owner != "Enemy")
            {
                //Hit an enemy here
                point.transform.GetComponent<Enemy>().Damage(m_damage);
            }
            else if (point.transform.tag == "Player" && m_owner != "Player")
            {
                GameObject.Find("Player").GetComponent<PlayerController>().Damage((int)m_damage);
                GameObject.Find("Player").GetComponent<PlayerController>().m_recentlyShot = true;
            }

            DestroyBullet(new Vector3(rot.x, rot.y, rot.x));
        })
        .SetEase(Ease.Linear);

        //Check if the transform still exists
        if(point.transform == null)
        {
            Destroy(this.gameObject);
        }

        yield return null;
    }

    public void DestroyBullet(Vector3 _negativeDirection)
    {
        Destroy(this.gameObject);
    }
}
