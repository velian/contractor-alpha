﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class Revolver : WeaponBase {

    public int m_currentCylinder = 0;
    public AudioClip m_overloadReload;

    GameObject m_laserDot;

    bool[] m_roundsLoaded;
    bool m_cooldown = false;
    bool m_leftTriggerDown = false;
    bool m_rightTriggerDown = false;
    
    void Awake()
    {
        if (!m_playerWeapon) return;

        m_ui = GameObject.FindWithTag("RevolverUI");
    }

    /// <summary>
    /// Start function; used to initialize all our custom non-derived values.
    /// </summary>
    void Start()
    {
        m_weaponType = WEAPON_TYPE.PISTOL;
        m_roundsLoaded = new bool[m_clipSize];

        if (m_playerWeapon)
        {
            //m_laserDot = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/LaserDot"));
            m_spreadMax = 0;
            m_overloadReload = Resources.Load<AudioClip>("Sounds/UI/switch30");        
        }
        //m_ui = GameObject.FindWithTag("RevolverUI");
    }

    /// <summary>
    /// Called each frame, handles weapon shooting and other mechanics weapon specific.
    /// </summary>
    public override void WeaponUpdate()
    {
        if (!m_playerWeapon) return;

        //If no ui
        if(m_ui == null)
        {
            m_ui = Utility.FindInactive("RevolverUI");
            //gameObject.SetActive(true);
        }

        if(GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.One) < 1)
        {
            m_leftTriggerDown = false;
        }
        if(GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.One) < 1)
        {
            m_rightTriggerDown = false;
        }

        if (!m_cooldown)
        {
            if (Input.GetMouseButtonDown(1) || GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.One) == 1 && !m_leftTriggerDown)
            {
                NextCylinder();
                m_ui.GetComponent<RevolverUI>().Rotate();
                AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 0.5f);
                StartCoroutine(ICooldown(m_fireCooldown));
                m_leftTriggerDown = true;
            }
            else if(Input.GetKeyDown(KeyCode.R) || GamePad.GetButtonDown(GamePad.Button.LeftShoulder, GamePad.Index.One))
            {
                if (m_roundsLoaded[m_currentCylinder] == false && m_ammunition > 0)
                {
                    m_ammunition--;
                    StartCoroutine(IReload());
                    StartCoroutine(ICooldown(m_reloadCooldown));
                }
                else
                {
                    NextCylinder();
                    m_ui.GetComponent<RevolverUI>().Rotate();
                    AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 0.5f);
                    StartCoroutine(ICooldown(m_reloadCooldown));
                }
            }
            else if (Input.GetMouseButtonDown(0) || GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.One) == 1 && !m_rightTriggerDown)
            {
                if (m_roundsLoaded[m_currentCylinder] == true)
                {
                    m_roundsLoaded[m_currentCylinder] = false;
                    m_ui.GetComponent<RevolverUI>().RemoveBullet(m_currentCylinder);
                    Fire();
                    StartCoroutine(ICooldown(m_fireCooldown));
                }
                else
                {
                    AudioSource.PlayClipAtPoint(m_gunEmpty, transform.position, 0.5f);
                    NextCylinder();
                    m_ui.GetComponent<RevolverUI>().Rotate();
                    StartCoroutine(ICooldown(m_fireCooldown));
                }

                m_rightTriggerDown = true;
            }
        }
    }

    IEnumerator ICooldown(float _timer)
    {
        m_cooldown = true;
        yield return new WaitForSeconds(_timer);
        m_cooldown = false;
    }

    IEnumerator IReload()
    {
        StartCoroutine(ICooldown(m_reloadCooldown));

        yield return new WaitForSeconds(0);
        AudioSource.PlayClipAtPoint(m_overloadReload, transform.position);
        m_roundsLoaded[m_currentCylinder] = true;
        m_ui.GetComponent<RevolverUI>().AddBullet(m_currentCylinder);        
    }

    public void NextCylinder()
    {
        m_currentCylinder = (m_currentCylinder == 0) ? m_clipSize - 1 : m_currentCylinder - 1;
    }
}
