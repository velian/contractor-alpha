﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class Carbine : WeaponBase {

    public int m_currentRound = 0;
    bool[] m_roundsLoaded;
    bool m_cocked = false;
    bool m_cooldown = false;

    void Awake()
    {
        m_ui = GameObject.FindWithTag("CarbineUI");
    }

    /// <summary>
    /// Start function; used to initialize all our custom non-derived values.
    /// </summary>
    void Start()
    {
        m_weaponType = WEAPON_TYPE.CARBINE;
        m_roundsLoaded = new bool[m_clipSize];
        m_ui = GameObject.FindWithTag("CarbineUI");

        if (m_playerWeapon)
        {
            m_spreadMax = 0;
        }        
    }

    void OnEnabled()
    {
        if (m_playerWeapon)
        {
            m_ui.GetComponent<CarbineUI>().UnCockRound();
        }  
    }

    /// <summary>
    /// Called each frame, handles weapon shooting and other mechanics weapon specific.
    /// </summary>
    public override void WeaponUpdate()
    {
        //If no ui
        if (m_ui == null)
        {
            m_ui = Utility.FindInactive("CarbineUI");
            //gameObject.SetActive(false);
        }

        if (m_cooldown) return;

        if (Input.GetKeyDown(KeyCode.R) || GamePad.GetButtonDown(GamePad.Button.LeftShoulder, GamePad.Index.One))
        {
            if (m_currentRound < m_clipSize && !m_reloading)
            {
                if (m_roundsLoaded[m_currentRound] == false && m_ammunition > 0)
                {
                    StartCoroutine(IReload());
                    AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 0.5f);
                    StartCoroutine(ICooldown(m_reloadCooldown));
                    m_ammunition--;                    
                }
            }
        }
        else if (Input.GetMouseButtonDown(1) || GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.One) == 1)
        {
            if (!m_cocked)
            {
                m_cocked = true;
                StartCoroutine(ICooldown(m_reloadCooldown));

                m_ui.GetComponent<CarbineUI>().CockRound();

                //Playsound
                AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 1.0f);
            }
        }
        else if (Input.GetMouseButtonDown(0) || GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.One) == 1)
        {
            if (m_currentRound > 0 && m_cocked)
            { 
                if (m_roundsLoaded[m_currentRound - 1] == true)
                {
                    m_roundsLoaded[m_currentRound - 1] = false;
                    Fire();
                    m_ui.GetComponent<CarbineUI>().RemoveBullet(m_currentRound - 1);
                    StartCoroutine(ICooldown(m_reloadCooldown));
                    
                    if(m_currentRound != 0)
                        NextRound();
                }
            }
            else if(m_cocked)
            {
                AudioSource.PlayClipAtPoint(m_gunEmpty, transform.position, 0.5f);
            }

            m_ui.GetComponent<CarbineUI>().UnCockRound();

            m_cocked = false;
        }
    }

    IEnumerator ICooldown(float _timer)
    {
        m_cooldown = true;
        yield return new WaitForSeconds(_timer);
        m_cooldown = false;
    }

    IEnumerator IReload()
    {
        m_reloading = true;
        yield return new WaitForSeconds(m_reloadCooldown);
        //print(m_currentRound + " was loaded!");
        m_roundsLoaded[m_currentRound] = true;
        m_ui.GetComponent<CarbineUI>().AddBullet(m_currentRound);
        
        if (m_currentRound == 0 && m_cocked)
        {
            m_ui.GetComponent<CarbineUI>().CockRound();
        }
        else if (!m_cocked)
        {
            m_ui.GetComponent<CarbineUI>().UnCockRound();
        }

        m_currentRound++;
        m_reloading = false;
    }

    void NextRound()
    {
        m_currentRound = (m_currentRound == 0) ? m_clipSize - 1 : m_currentRound - 1;
    }
}
