﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class Shotgun : WeaponBase {

    bool m_leftRoundLoaded = false;
    bool m_rightRoundLoaded = false;

    bool m_leftTriggerDown = false;
    bool m_rightTriggerDown = false;

    void Awake()
    {
        m_ui = GameObject.FindWithTag("ShotgunUI");
    }

    /// <summary>
    /// Start function; used to initialize all our custom non-derived values.
    /// </summary>
    void Start()
    {
        m_weaponType = WEAPON_TYPE.SHOTGUN;
        m_ui = GameObject.FindWithTag("ShotgunUI");
    }

    /// <summary>
    /// Called each frame, handles weapon shooting and other mechanics weapon specific.
    /// </summary>
    public override void WeaponUpdate()
    {
        //If no ui
        if (m_ui == null)
        {
            m_ui = Utility.FindInactive("ShotgunUI");
            //gameObject.SetActive(false);
        }

        if(GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.One) < 1)
        {
            m_leftTriggerDown = false;
        }
        if(GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.One) < 1)
        {
            m_rightTriggerDown = false;
        }

        //Reload on R key
        if (Input.GetKeyDown(KeyCode.R) || GamePad.GetButtonDown(GamePad.Button.LeftShoulder, GamePad.Index.One))
        {
            if (!m_leftRoundLoaded && !m_reloading && m_ammunition > 0)
            {       
                StartCoroutine(IReload(0));
                AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 0.5f);
                m_leftRoundLoaded = true;
            }
            else if (!m_rightRoundLoaded && !m_reloading && m_ammunition > 0)
            {
                StartCoroutine(IReload(1));
                AudioSource.PlayClipAtPoint(m_gunReload, transform.position, 0.5f);
                m_rightRoundLoaded = true;
            }
        }

        if (Input.GetMouseButtonDown(0) || GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.One) == 1 && !m_leftTriggerDown) //Fire Left Trigger
        {
            m_leftTriggerDown = true;

            if(m_leftRoundLoaded)
            {
                m_leftRoundLoaded = false;
                Fire();
                m_ui.GetComponent<ShotgunUI>().RemoveBullet(0);
            }
            else
            {
                AudioSource.PlayClipAtPoint(m_gunEmpty, transform.position, 0.5f);
            }
        }

        if (Input.GetMouseButtonDown(1) || GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.One) == 1 && !m_rightTriggerDown) //Fire Left Trigger
        {
            m_rightTriggerDown = true;

            if (m_rightRoundLoaded)
            {
                m_rightRoundLoaded = false;
                Fire();
                m_ui.GetComponent<ShotgunUI>().RemoveBullet(1);
            }
            else
            {
                AudioSource.PlayClipAtPoint(m_gunEmpty, transform.position, 0.5f);
            }
        }
    }

    IEnumerator IReload(int _roundNumber)
    {
        m_reloading = true;
        yield return new WaitForSeconds(m_reloadCooldown);
        //print(m_currentRound + " was loaded!");
        m_ui.GetComponent<ShotgunUI>().AddBullet(_roundNumber);
        m_ammunition--;
        m_reloading = false;
    }

    //void NextRound()
    //{
        //m_currentRound = (m_currentRound == 0) ? m_clipSize - 1 : m_currentRound - 1;
    //}
}
