﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Credit : MonoBehaviour {

    public float m_minimumDistance = 3f;
    public int m_creditAmount = 10;

    GameObject m_player;    
    bool m_triggered = false;
    bool m_coroutineRunning = false;

    public void Initialize()
    {
        m_player = GameObject.Find("Player");
    }
	
	// Update is called once per frame
	void Update()
    {
	    //if(Vector3.Distance(transform.position, m_player.transform.position) < m_minimumDistance)
        //{
            if (!m_triggered)
            {
                StartCoroutine(ISpawnAnimation(1f));
                m_triggered = true;
            }
        //}
	}

    IEnumerator ISpawnAnimation(float _idleDuration)
    {
        yield return new WaitForSeconds(_idleDuration);

        Destroy(GetComponent<BoxCollider>());

        //DoTween
        Vector3 originPos = transform.position;
        Vector3 offset = new Vector3(0, 1f, 0);
        float duration = 1f;
        for (float i = 0; i < duration; i += Time.deltaTime * duration)
        {
            Mathf.Clamp(i, 0, duration);

            float percentage = i;
            percentage = Mathf.Pow(percentage, 2f);
            transform.position = Vector3.Lerp(originPos, m_player.transform.position + offset, percentage);
            yield return null;
        } 

        PlayerInventory.m_manager.GetComponent<PlayerInventory>().m_credits += m_creditAmount;
        Destroy(this.gameObject);
    }
}
