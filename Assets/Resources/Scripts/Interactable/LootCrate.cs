﻿using UnityEngine;
using System.Collections;

public class LootCrate : Interactable {

    public GameObject m_shopCanvas;
    public GameObject m_emitter = null;
	
	// Update is called once per frame
    public override void ObjectUpdate()
    {
	    if(m_triggered)
        {
            if(m_emitter == null)
            {
                //Generate emitter
                int randomNum = Random.Range(0, 2);
                m_emitter = (randomNum == 0) ? Resources.Load<GameObject>("Prefabs/Scene/CreditEmitter") : Resources.Load<GameObject>("Prefabs/Scene/AmmoEmitter");
            }
            
            ActivateEmitter();

            Destroy(this.gameObject);
        }
	}

    void ActivateEmitter()
    {
        GameObject emitter = (GameObject)Instantiate(m_emitter, transform.position, transform.rotation);

        if (emitter.GetComponent<CreditEmitter>())
        {
            emitter.GetComponent<CreditEmitter>().Emit();
        }
        else if (emitter.GetComponent<AmmoEmitter>())
        {
            emitter.GetComponent<AmmoEmitter>().Emit();
        }
    }
}
