﻿using UnityEngine;
using System.Collections;

public class AmmoEmitter : MonoBehaviour {

    public int m_numberOfRounds = 10;
    public float m_duration = 0.5f;
    public GameObject m_ammunition = null;

    // Update is called once per frame
    public void Emit()
    {
        if (m_ammunition == null)
        {
            int randomType = Random.Range(0, 3);

            switch (randomType)
            {
                case 0:
                    m_ammunition = Resources.Load<GameObject>("Prefabs/Ammo/RevolverAmmo");
                    break;
                case 1:
                    m_ammunition = Resources.Load<GameObject>("Prefabs/Ammo/CarbineAmmo");
                    break;
                case 2:
                    m_ammunition = Resources.Load<GameObject>("Prefabs/Ammo/ShotgunAmmo");
                    break;
            }
        }

        for (int i = 0; i < m_numberOfRounds; i++)
        {
            GameObject particle = (GameObject)Instantiate(m_ammunition, transform.position, m_ammunition.transform.rotation);
            particle.transform.parent = this.transform;
            particle.transform.parent = null;
        }

        //Kill parent loot crate
        if (transform.parent != null)
            Destroy(transform.parent.gameObject);
    }
}
