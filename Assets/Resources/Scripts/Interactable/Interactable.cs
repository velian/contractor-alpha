﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using DG.Tweening;

public class Interactable : MonoBehaviour {

    public bool m_triggered = false;

    public KeyCode m_keyToPress = KeyCode.E;

    GameObject m_icon;
    Vector2 m_scale = new Vector2(0.5f,0.5f);

    protected GameObject m_selfIcon;

    public float m_distance = 1.5f;
    
    protected uint m_screenWidth;
    protected uint m_screenHeight;    

    protected Transform m_player;

    protected bool m_inRange = false;

	// Use this for initialization
	void Start()
    {
        m_screenWidth =  (uint)Screen.width;
        m_screenHeight = (uint)Screen.height;
        m_player = GameObject.FindWithTag("Player").transform;

        //Instantiate interact icon
        m_icon = Resources.Load<GameObject>("Prefabs/Interact");

        m_selfIcon = Instantiate(m_icon) as GameObject;
        m_selfIcon.transform.SetParent(GameObject.FindWithTag("WorldSpaceCanvas").transform);
        m_selfIcon.GetComponent<WorldSpaceUI>().Initialize(transform, m_scale);
        m_selfIcon.SetActive(false);
	}
	    
	// Update is called once per frame
	void Update()
    {
        if (Vector3.Distance(transform.position, m_player.transform.position) < m_distance)
        {
            //Set show pop-up
            if (!m_inRange)
            {
                m_selfIcon.SetActive(true);
                m_triggered = false;
                m_inRange = true;
            }

            //If key pressed and pop-up
            if (Input.GetKeyDown(m_keyToPress) || Input.GetButtonDown("Submit") || GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.A, GamepadInput.GamePad.Index.One))
            {
                m_triggered = true;
                m_selfIcon.SetActive(false);
            }

            ObjectUpdate();
        }
        else if (m_inRange)
        {
            m_selfIcon.SetActive(false);
            m_inRange = false;
        }
       
	}

    public virtual void ObjectUpdate()
    {

    }
}
