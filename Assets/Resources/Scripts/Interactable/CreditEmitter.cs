﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CreditEmitter : MonoBehaviour {

    public int m_numberOfCredits = 10;
    public float m_duration = 0.5f;
    public GameObject m_creditPrefab;
	
	// Update is called once per frame
	public void Emit()
    {
        for (int i = 0; i < m_numberOfCredits; i++ )
        {
            GameObject particle = (GameObject)Instantiate(m_creditPrefab, transform.position, m_creditPrefab.transform.rotation);
            particle.transform.parent = this.transform;
            particle.GetComponent<Credit>().Initialize();
            particle.transform.parent = null;
        }

        //Kill parent loot crate
        if (transform.parent != null)
            Destroy(transform.parent.gameObject);
	}
}
