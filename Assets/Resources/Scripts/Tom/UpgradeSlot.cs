﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UpgradeSlot : MonoBehaviour
{
    //Private vars
    //---------------------------------
    int m_shield = 0;
    int m_damage = 0;
    WeaponBase.WEAPON_TYPE m_weaponID = 0;

    int m_weaponPrice = 0;
    int m_sellPrice = 0;
    int m_shieldUpgradeCost = 120;
    int m_damageUpgradeCost = 100;

    bool m_equiped;

    GameObject m_shieldStatsBar;
    GameObject m_damageStatsBar;
    GameObject m_playerInv;
    WeaponConfiguration m_weaponConfig;
    //---------------------------------

    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Initialises the slot with data from the weaponOBJ
    /// </summary>
    /// <param name="_weaponObj"></param>
    public void Initialise(WeaponConfiguration _weaponConfig)
    {
        m_playerInv = GameObject.FindWithTag("Player");
        m_weaponConfig = _weaponConfig;

        m_equiped = m_weaponConfig.weaponEquipped;

        m_shield = (int)m_weaponConfig.weaponAttributes.m_shield;
        m_damage = (int)m_weaponConfig.weaponAttributes.m_damage;
        m_weaponID = m_weaponConfig.weaponType;
        m_weaponPrice = (int)m_weaponConfig.weaponCost;

        //Setting the Stat bar gameObjects
        m_shieldStatsBar = transform.FindChild("StatBars").transform.FindChild("ShieldBar").gameObject;
        m_damageStatsBar = transform.FindChild("StatBars").transform.FindChild("DamageBar").gameObject;

        //Displaying the current stats level
        UpdateStatsBar("ShieldBar", m_shield);
        UpdateStatsBar("DamageBar", m_damage);

        //Setting the default cost for the fist upgrade
        m_shieldUpgradeCost = CalculateUpgradeCost(m_shield, m_weaponConfig.weaponBaseCost / 2);
        m_damageUpgradeCost = CalculateUpgradeCost(m_damage, m_weaponConfig.weaponBaseCost / 2);
 
        //Setting the text to equip or equipped
        if (m_weaponConfig.weaponEquipped)
            transform.FindChild("EquipButton").transform.FindChild("Text").GetComponent<Text>().text = "Equipped";
        else
            transform.FindChild("EquipButton").transform.FindChild("Text").GetComponent<Text>().text = "Equip";

        //Get the cost of each upgrade from the weapon.
        ChangeGunTypeText();
        CalculateSellPrice();
        UpdateSlotGUI(); 
    }

    /// <summary>
    /// This will change the text showing the type of the gun
    /// </summary>
    void ChangeGunTypeText()
    {
        Sprite weaponImage = Resources.Load<Sprite>("Textures/UI/Bullet");
        string typeName = "ERROR";
        switch (m_weaponID)
        {
            case WeaponBase.WEAPON_TYPE.NONE:
                {
                    typeName = "NoTypeFound";
                } break;
            case WeaponBase.WEAPON_TYPE.PISTOL:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_Revolver");
                    typeName = "Pistol";
                } break;
            case WeaponBase.WEAPON_TYPE.SHOTGUN:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_Sawn-of");
                    typeName = "Shotgun";
                } break;
            case WeaponBase.WEAPON_TYPE.CARBINE:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_shotgun");
                    typeName = "Carbine";
                } break;
           
        }
        transform.FindChild("WeaponImage").GetComponent<Image>().sprite = weaponImage;
        transform.FindChild("WeaponTypeText").transform.FindChild("Text").GetComponent<Text>().text = typeName;
    }
    
    /// <summary>
    /// Sets the stat of the shield
    /// </summary>
    /// <param name="_shield"></param>
    public void BuyShieldStat(int _shield)
    {
        //Checks to see if you have enough money to buy the upgrade
       if (m_shieldUpgradeCost > m_playerInv.GetComponent<PlayerInventory>().m_credits)
            return;
        else
            m_playerInv.GetComponent<PlayerInventory>().m_credits -= m_shieldUpgradeCost;

        m_shield += _shield;
        if (m_shield > 10) return;

        UpdateStatsBar("ShieldBar", m_shield); //Update the colour of the stats bar if an upgrade.
        
        //Set the weapons values
        m_weaponConfig.weaponAttributes.m_shield = (uint)m_shield;

        //Calculate the new cost and update the Slots GUI to display the new price
        m_shieldUpgradeCost = CalculateUpgradeCost(m_shield, m_shieldUpgradeCost);
        UpdateSlotGUI();
        CalculateSellPrice();
    }

    /// <summary>
    /// Sets the stat of the damage
    /// </summary>
    /// <param name="_damage"></param>
    public void BuyDamageStat(int _damage)
    {
        //Checks to see if you have enough money to buy the upgrade
        if (m_damageUpgradeCost > m_playerInv.GetComponent<PlayerInventory>().m_credits)
            return;
        else
            m_playerInv.GetComponent<PlayerInventory>().m_credits -= m_damageUpgradeCost;

        m_damage += _damage;
        if (m_damage > 10) return;

        UpdateStatsBar("DamageBar", m_damage);//Update the colour of the stats bar if an upgrade.

        //Set the weapons values
        m_weaponConfig.weaponAttributes.m_damage = (uint)m_damage;

        //Calculate the new cost and update the Slots GUI to display the new price
        m_damageUpgradeCost = CalculateUpgradeCost(m_damage, m_damageUpgradeCost);
        UpdateSlotGUI();
        CalculateSellPrice();
       }

    /// <summary>
    /// If someone clicks the sell button
    /// </summary>
    public void SellWeapon()
    {
        m_playerInv.GetComponent<PlayerInventory>().m_credits += m_sellPrice;

        m_playerInv.GetComponent<PlayerInventory>().m_weaponList.Remove(m_weaponConfig);
        GameObject.Find("ShopCanvas").GetComponent<ShopCanvas>().CreateUpgradeSlots();
        DestroyObject(this);
    }

    /// <summary>
    /// When someone presses the equip button.
    /// </summary>
    public void EquipWeapon()
    {
        if (m_equiped)
        {
            transform.FindChild("EquipButton").transform.FindChild("Text").GetComponent<Text>().text = "Equip";
            m_equiped = false;
        }
        else
        {
            transform.FindChild("EquipButton").transform.FindChild("Text").GetComponent<Text>().text = "Equipped";
            m_equiped = true;
        }

        m_weaponConfig.weaponEquipped = m_equiped;
    }

    /// <summary>
    /// This Updates the canvas to show how upgraded you weapon is based of the stats
    /// </summary>
    void UpdateStatsBar(string _statBarName, int _stat)
    {
        GameObject Stat = transform.FindChild("StatBars").transform.FindChild(_statBarName).gameObject;
        if (Stat == null)
        {
            print("NO STAT BAR FOUND");
            return;
        }

        //Find the upgrade point
        GameObject statPoints = Stat.transform.FindChild("Points").gameObject;

        for (int i = 1; i <= _stat; i++)
        {
            if (i > 10) //There are only 10 points to upgrade
                break;
        
            //Change the colour of each point if you have upgraded it
            GameObject point;
            point = statPoints.transform.FindChild("Point" + i).gameObject;
            point.GetComponent<Image>().color = new Vector4(0, 255, 86, 255);
        }
    }
    
    /// <summary>
    /// Calculates the cost for the next upgrade for a specific stat
    /// </summary>
    /// <param name="_currStatLevel"></param>
    /// <param name="_currCost"></param>
    /// <returns></returns>
    int CalculateUpgradeCost(int _currStatLevel, int _currCost)
    {
        float cost = 0;
        int basePrice = m_weaponPrice / 2;

        for (int i = 1; i <= _currStatLevel; i++)
        { 
            cost += basePrice + basePrice * ((float)i / 10.0f);
        }
        return (int)cost;        
    }

    /// <summary>
    /// Calculates the sell price for a weapon based off its stats
    /// </summary>
    void CalculateSellPrice()
    {
        m_sellPrice = 0;
        //Calc Sell Price
        float totalUpgrade = m_damage + m_shield;

        m_weaponConfig.CalculateValue();
        int currValue = m_weaponConfig.weaponCost;

        m_sellPrice = (int)((float)currValue - (float)currValue * 0.2f);

        transform.FindChild("SellButton").transform.FindChild("Text").GetComponent<Text>().text = m_sellPrice.ToString();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
    }

    /// <summary>
    /// Sets the add button to MAXED if fully upgraded
    /// </summary>
    void UpdateSlotGUI()
    {
        //Set the sell button to inactive if its the first gun
        if (m_weaponConfig.weaponSellable == false)
            transform.FindChild("SellButton").gameObject.GetComponent<Button>().interactable = false;

        //Display the stats next upgrade price if its not maxed out.
        if (m_shield < 10)
            m_shieldStatsBar.transform.FindChild("Heading").transform.FindChild("Cost").transform.FindChild("Text").GetComponent<Text>().text = m_shieldUpgradeCost.ToString();
        else
        {
            m_shieldStatsBar.transform.FindChild("Heading").transform.FindChild("Cost").transform.FindChild("Text").GetComponent<Text>().text = "MXD";
            m_shieldStatsBar.transform.FindChild("Heading").transform.FindChild("AddBtn").gameObject.GetComponent<Button>().interactable = false;
        }
        if (m_damage < 10)
            m_damageStatsBar.transform.FindChild("Heading").transform.FindChild("Cost").transform.FindChild("Text").GetComponent<Text>().text = m_damageUpgradeCost.ToString();
        else
        {
            m_damageStatsBar.transform.FindChild("Heading").transform.FindChild("Cost").transform.FindChild("Text").GetComponent<Text>().text = "MXD";
            m_damageStatsBar.transform.FindChild("Heading").transform.FindChild("AddBtn").gameObject.GetComponent<Button>().interactable = false;
        }  
    }
}
