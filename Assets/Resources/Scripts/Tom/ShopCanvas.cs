﻿//-----------------------------------------------------------------------------
//Author: Tommas Solarino   
//Description: This is attached to the shop canvas and is responsible for 
//      creating the weapon tabs and handling upgrades and selling
//-----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopCanvas : MonoBehaviour 
{
    //Public var
    //-------------------------------------------------------------------------
    public GameObject m_upgradeCanvas;
    public GameObject m_buyCanvas;
    public List<WeaponConfiguration> m_BuyableWeapons = new List<WeaponConfiguration>();
    public int m_credits;
    //-------------------------------------------------------------------------
    
    //Private vars
    //-------------------------------------------------------------------------
    GameObject m_playerInv;
    GameObject m_upgradeSlotTemplate;
    GameObject m_buySlotTemplate;
    List<GameObject> m_buySlotList = new List<GameObject>();
    List<GameObject> m_upgradeSlotList = new List<GameObject>();
    List<WeaponConfiguration> m_allWeapons = new List<WeaponConfiguration>();
    
    float m_slotOffsetX = 450;
    float m_slotOffsetY = 180;
    //-------------------------------------------------------------------------

    void Start () 
    {
        m_playerInv = GameObject.FindWithTag("Player");
        if (m_playerInv == null)
            print("couldn't find player");

        this.gameObject.SetActive(false);
	}

    void OnEnable()
    {
        //Setting the slot templates
        m_upgradeSlotTemplate = m_upgradeCanvas.transform.FindChild("UpgradeSlotTemplate").gameObject;
        m_buySlotTemplate = m_buyCanvas.transform.FindChild("BuySlotTemplate").gameObject;

        CreateUpgradeSlots();
        CreateBuySlots();

        m_credits = m_playerInv.GetComponent<PlayerInventory>().m_credits;
    }
	
    /// <summary>
    /// Creates the upgrade slots for the shop based off the weapons the player has
    /// </summary>
    public void CreateUpgradeSlots()
    {
        //checking for the player object
        if (m_playerInv == null)
            m_playerInv = GameObject.FindWithTag("Player");

       //Removing old upgrade slots and adding the new ones incase of changes
        foreach (var obj in m_upgradeSlotList)
        {
            DestroyObject(obj);
        }
        m_upgradeSlotList.Clear();

        m_allWeapons.Clear();

        m_allWeapons.AddRange(m_playerInv.GetComponent<PlayerInventory>().m_weaponList);
       
        GameObject cloneSlot;
        float offsetX = m_slotOffsetX;
        float offsetY = m_slotOffsetY;

        foreach (var weapon in m_allWeapons)
        {
            cloneSlot = Instantiate(m_upgradeSlotTemplate);
            cloneSlot.GetComponent<UpgradeSlot>().Initialise(weapon);
            cloneSlot.transform.SetParent(m_upgradeCanvas.transform);
            
            m_upgradeSlotList.Add(cloneSlot);

            PositionSlots(ref offsetX, ref offsetY, cloneSlot, m_upgradeSlotTemplate);
        }
    }

    /// <summary>
    /// Creates the buy weapon slots from a saved list of randomized weapons
    /// </summary>
    public void CreateBuySlots()
    {
        //regen the list of weapons
        GenerateBuyableWeapons();

        GameObject cloneSlot;
        float offsetX = m_slotOffsetX;
        float offsetY = m_slotOffsetY;

        foreach (var obj in m_buySlotList)
        {
            DestroyObject(obj);
        }
        m_buySlotList.Clear();

        foreach (var weaponConfig in m_BuyableWeapons)
        {
            //Seting up the Buy slot
            //-----------------------------------------------------------------
            cloneSlot = Instantiate(m_buySlotTemplate);
            cloneSlot.GetComponent<BuySlot>().Initialise(weaponConfig);
            cloneSlot.transform.SetParent(m_buyCanvas.transform);
            m_buySlotList.Add(cloneSlot);

            PositionSlots(ref offsetX, ref offsetY, cloneSlot, m_buySlotTemplate);
        }
    }

    /// <summary>
    /// Generates random wepaons for the buy tab
    /// </summary>
    public void GenerateBuyableWeapons()
    {
        //TODO::
        //load weapon saves, if not saves generate below and save
        //if save, remove a few guns and randomise them
        //Maybe change the randon code to a little function.
        //Change this function name to loadBuyableweapons and make another randomise function

        int weaponCount = Random.Range(1, 9);
        m_BuyableWeapons.Clear();

        for (int i = 0; i < weaponCount; i++)
        {
            //Randomize weapon values
            int randDamage = Random.Range(1, 10);
            int randSheild = Random.Range(1, 10);
            WeaponBase.WEAPON_TYPE weaponType = (WeaponBase.WEAPON_TYPE)Random.Range(1, 4);

            //Set new weapon with values
            WeaponConfiguration weaponConfig = new WeaponConfiguration();
            weaponConfig.Initialize(null, weaponType, new WeaponBase.Attributes(randDamage, randSheild), 0, true);

            //Add weapon config to the list
            m_BuyableWeapons.Add(weaponConfig);
        }
    }

    public void PositionSlots(ref float _xOffset, ref float _yOffset, GameObject _slotClone, GameObject _slotTemplate)
    {
        float cPosX = 0;
        float cPosY = 0;
        float cPosZ = 0;

        _slotClone.SetActive(true);

        if (_xOffset == m_slotOffsetX)
        {
            _xOffset = 0;
            _yOffset -= m_slotOffsetY;
        }
        else
            _xOffset = m_slotOffsetX;

        cPosX = _slotTemplate.transform.position.x + _xOffset;
        cPosY = _slotTemplate.transform.position.y + _yOffset;
        cPosZ = _slotTemplate.transform.position.z;


        _slotClone.transform.localScale = _slotTemplate.transform.localScale;
        _slotClone.transform.position = new Vector3(cPosX, cPosY, cPosZ);
    }
    
	// Update is called once per frame
	void Update () 
    {
        m_credits = m_playerInv.GetComponent<PlayerInventory>().m_credits;
        transform.FindChild("Credits").transform.FindChild("Text").GetComponent<Text>().text = m_credits.ToString();
	}
}
