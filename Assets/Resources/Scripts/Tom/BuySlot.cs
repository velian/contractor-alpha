﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuySlot : MonoBehaviour {

    //Vars
    GameObject m_playerInv;
    WeaponConfiguration m_weaponConfig;
    int m_buyPrice;
	// Use this for initialization
	void Start () {
	
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_weaponObj"></param>
    public void Initialise(WeaponConfiguration _weaponConfig)
    {
        m_playerInv = GameObject.FindWithTag("Player");
        m_weaponConfig = _weaponConfig;

        m_buyPrice = m_weaponConfig.weaponCost;

        UpdateStatsBar("Damage", (int)m_weaponConfig.weaponAttributes.m_damage);
        UpdateStatsBar("Shield", (int)m_weaponConfig.weaponAttributes.m_shield);
        ChangeGunTypeText();
    }

    void UpdateStatsBar(string _statBarName, int _stat)
    {
        GameObject Stat = transform.FindChild("StatBars").transform.FindChild(_statBarName).gameObject;
        if (Stat == null)
        {
            print("NO STAT BAR FOUND");
            return;
        }

        //Find the upgrade point
        GameObject statPoints = Stat.transform.FindChild("Points").gameObject;

        for (int i = 1; i <= _stat; i++)
        {
            if (i > 10) //There are only 10 points to upgrade
                break;

            //Change the colour of each point if you have upgraded it
            GameObject point;
            point = statPoints.transform.FindChild("Point" + i).gameObject;
            point.GetComponent<Image>().color = new Color(0.4f, 0.5f, 1.0f, 0.5f);
        }
    }

    void ChangeGunTypeText()
    {
        Sprite weaponImage = Resources.Load<Sprite>("Textures/UI/Bullet");
        string typeName = "ERROR";
        switch (m_weaponConfig.weaponType)
        {
            case WeaponBase.WEAPON_TYPE.NONE:
                {
                    typeName = "NoTypeFound";
                } break;
            case WeaponBase.WEAPON_TYPE.PISTOL:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_Revolver");
                    typeName = "Pistol";
                } break;
            case WeaponBase.WEAPON_TYPE.SHOTGUN:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_Sawn-of");
                    typeName = "Shotgun";
                } break;
            case WeaponBase.WEAPON_TYPE.CARBINE:
                {
                    weaponImage = Resources.Load<Sprite>("Textures/UI/Icon_shotgun");
                    typeName = "Carbine";
                } break;

        }
        transform.FindChild("WeaponImage").GetComponent<Image>().sprite = weaponImage;
        transform.FindChild("WeaponTypeText").transform.FindChild("Text").GetComponent<Text>().text = typeName;

        m_weaponConfig.CalculateValue();
        transform.FindChild("BuyButton").transform.FindChild("Text").GetComponent<Text>().text = m_weaponConfig.weaponCost.ToString();
    }

    public void BuyGun()
    {
        //recalc the cost
        m_weaponConfig.CalculateValue();
        m_buyPrice = m_weaponConfig.weaponCost;

        //Do nothing if the player doesnt have enough money
        if (m_buyPrice > m_playerInv.GetComponent<PlayerInventory>().m_credits)
            return;

        m_playerInv.GetComponent<PlayerInventory>().m_credits -= m_buyPrice;
        m_playerInv.GetComponent<PlayerInventory>().m_weaponList.Add(m_weaponConfig);
        transform.FindChild("BuyButton").GetComponent<Button>().interactable = false;
        //TODO: 
        // - Add this weapon to bought weapons AKA playerinv weapons list
        //Remove this m_weoponConfig from the shop canvas
        //Reposition all the weapon slots
        //remove this objects AKA the buy Slot

        GameObject.Find("ShopCanvas").GetComponent<ShopCanvas>().m_BuyableWeapons.Remove(m_weaponConfig);
        //GameObject.Find("ShopCanvas").GetComponent<ShopCanvas>().PositionSlots();
        DestroyObject(this);

        //TODO: Give the player this weapon
    }

	// Update is called once per frame
	void Update () 
    {
	
	}
}
