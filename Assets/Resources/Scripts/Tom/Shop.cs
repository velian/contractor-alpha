﻿//-----------------------------------------------------------------------------
//Author: Tommas solarino  
//Description: This script would be attached to a shop object that when the 
//  player presses e on will turn on the shop canvas and handle everything the
//  player does in the shop.
//-----------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class Shop : Interactable 
{
    //Public Vars
    //---------------------------------
    //public float m_accessDistance = 2; //how close the player needs to be to the shop ogj to access the shop
    public GameObject m_shopCanvas;
    //---------------------------------

    //Private Vars
    //---------------------------------
    //GameObject player;
    //float distToPlayer;
    bool m_initialized = false;
    //---------------------------------

    void StartUp()
    {
        m_shopCanvas = GameObject.Find("ShopCanvas");
            
        if (m_shopCanvas == null)
            print("You need to add a shop canvas to the shop object!!");
    }

    void OnEnabled()
    {
    }

    /// <summary>
    /// Called under interactable; manages 'E' hover text and the like
    /// </summary>
    public override void ObjectUpdate()
    {
        if(!m_initialized)
        {
            m_initialized = true;
        }

        if (Input.GetKeyDown(m_keyToPress))
        {
        }

        //If we press E on this thing
        if (Input.GetKeyDown(m_keyToPress) && m_triggered || Input.GetButtonDown("Submit"))
            m_shopCanvas.SetActive(!m_shopCanvas.activeSelf);
    }
}