﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Tile : MonoBehaviour
{
    public GameObject m_doorPrefab;
    public float m_lerpTime = 0.5f;
    private bool m_lerping = false;

    void Start()
    {

    }

    /// <summary>
    /// Reveal all the walls in our tile.
    /// </summary>
    public void RevealWall()
    {
        StopAllCoroutines();
        StartCoroutine(ERevealWall());
    }

    /// <summary>
    /// Hide all the walls in our tile.
    /// </summary>
    /// <param name="_alphaMin">Alpha minimum to lerp to.</param>
    public void HideWall(float _alphaMin)
    {
        StopAllCoroutines();
        StartCoroutine(EHideWall(_alphaMin));
    }

    IEnumerator ERevealWall()
    {
        foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
            obj.GetComponent<SpriteRenderer>().material.DOColor(Color.white, m_lerpTime);

        yield return null;

        //Color color = GetComponentInChildren<Renderer>().material.color;
        //
        //for (float i = color.a; i < 1; )
        //{
        //    i += Time.deltaTime;
        //    foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
        //        obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
        //    yield return null;
        //}
        //yield return null;
    }    

    IEnumerator EHideWall(float _alphaMin)
    {
        //Color color = GetComponentInChildren<Renderer>().material.color;

        foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
            obj.GetComponent<SpriteRenderer>().material.DOColor(new Color(1, 1, 1, _alphaMin), m_lerpTime);

        //if (color.a < _alphaMin)
        //{
        //    for (float i = color.a; i < _alphaMin; )
        //    {
        //        i += Time.deltaTime;
        //        foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
        //            obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
        //        yield return null;
        //    }
        //}
        //else
        //{
        //    for (float i = color.a; i > _alphaMin; )
        //    {
        //        i -= Time.deltaTime;
        //        foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
        //            obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
        //        yield return null;
        //    }
        //}
        yield return null;
    }

    /// <summary>
    /// If the player enters this trigger, reveal all the walls.
    /// </summary>
    /// <param name="other">Object that enters the trigger.</param>
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().SetRoom(GetComponentInParent<Room>());
            GetComponentInParent<Room>().RevealWalls();
        }
    }
}
