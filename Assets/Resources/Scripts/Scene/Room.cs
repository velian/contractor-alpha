﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour
{
    public bool m_hideOnAwake = true;
    public bool m_startingRoom = false;

    public List<Room> m_roomConnections = null;
    public List<Enemy> m_inhabitants = null;

    public bool m_connected = false;

    public int m_index = 0;

    protected bool m_gray = false;
    protected List<Tile> m_children = null;
    

    bool initialized = false;

    // Use this for initialization
    void Start()
    {
        if (!initialized)
            Initialize();
    }

    /// <summary>
    /// Initialize call, ability to init instead of being forced to use "void Start()"
    /// </summary>
    public void Initialize()
    {
        initialized = true;

        if(RoomGenerator.m_currentIndex != 0)
        {
            m_startingRoom = false;
        }

       // if(previousRoom != null)
       // {
            //m_roomConnections.Add(previousRoom);
       // }

        m_children = new List<Tile>();        
        m_children.AddRange(GetComponentsInChildren<Tile>());

        m_inhabitants = new List<Enemy>();
        m_inhabitants.AddRange(GetComponentsInChildren<Enemy>());

        foreach(Enemy inhabitant in m_inhabitants)
        {
            inhabitant.SetRoom(this);
        }

        if (m_startingRoom || !m_hideOnAwake)
        {
            foreach (Tile obj in m_children)
            {
                obj.RevealWall();
            }

            if (m_roomConnections != null && m_startingRoom)
            {
                foreach (Room neighbor in m_roomConnections)
                {
                    if (!neighbor.m_gray)
                    {
                        if (neighbor.m_children != null)
                        {
                            if(neighbor.GetComponentsInChildren<Tile>() != null)
                                neighbor.m_children.AddRange(neighbor.GetComponentsInChildren<Tile>());
                        }

                        neighbor.HideWalls(0.1f);
                        neighbor.m_gray = true;
                    }
                }
            }
        }
        else
        {
            if (!m_gray)
                HideWalls(0);
        }
    }

    /// <summary>
    /// Begins a coroutine on all the tiles that increases alpha value on the renderer's colour
    /// </summary>
    public void RevealWalls()
    {
        if (m_children != null)
        {
            foreach (Tile tile in m_children)
            {
                tile.RevealWall();
            }
        }

        foreach (Room room in m_roomConnections)
        {
            room.HideWalls(0.2f);
            room.m_gray = true;
        }

        if (m_inhabitants != null)
        {
            foreach (Enemy enemy in m_inhabitants)
            {
                enemy.RevealEnemy();
            }
        }
    }

    /// <summary>
    /// Lerp the alpha of all the tiles in our room to the given minimum alpha value.
    /// </summary>
    /// <param name="_alphaMin">The minimum alpha at which to lerp to in the coroutine</param>
    public void HideWalls(float _alphaMin)
    {
        if (m_children != null)
        {
            foreach (Tile tile in m_children)
            {
                tile.HideWall(_alphaMin);
            }
        }

        if (m_inhabitants != null)
        {
            if (m_inhabitants.Count > 0)
            {
                foreach (Enemy enemy in m_inhabitants)
                {
                    if(!enemy.m_alerted && enemy.m_justAlerted == true)
                        enemy.HideEnemy(_alphaMin);
                }
            }
        }
    }
}