﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public static GameObject manager;

    public AudioClip[] m_musicList;

	// Use this for initialization
	void Start ()
    {
        if (m_musicList.Length < 0) return;

        if (Application.loadedLevel == 0)
        {
            AudioSource.PlayClipAtPoint(m_musicList[0], Vector3.zero, 0.5f);
        }
        else
        {
            int randomMusicIndex = Random.Range(1, m_musicList.Length);
            AudioSource.PlayClipAtPoint(m_musicList[randomMusicIndex], Vector3.zero, 0.5f);
        }

        if (manager == null)
        {
            DontDestroyOnLoad(gameObject);
            manager = gameObject;
        }
        else if(manager != gameObject)
        {
            Destroy(gameObject);
        }
	}

    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    Application.LoadLevel(Application.loadedLevel + 1);
        //}
    }
}
