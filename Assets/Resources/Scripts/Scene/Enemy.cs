﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Enemy : MonoBehaviour {

    public GameObject m_ragdoll = null;
    public GameObject m_emitter;
    public float m_health;
    public bool m_alerted = false;
    public bool m_justAlerted = true;

    [Header("Sound Effects")]
    public AudioClip m_alertSound;
    public AudioClip m_lostSound;
    public AudioClip m_shootTalk;

    [Space(6)]
    public float m_searchCooldown = 5.0f;
    public float m_chatterCooldown = 2.0f;
    
    public Room m_currentRoom;

    public bool m_activeAI = true;

    float m_chatterCooldownOrigin;
    float m_searchCooldownOrigin;

    GameObject m_player;
    
    NavMeshAgent m_agent;
    bool m_firing = false;

	// Use this for initialization
	void Start ()
    {
        m_player = GameObject.Find("Player");
        m_agent = GetComponent<NavMeshAgent>();
        m_chatterCooldownOrigin = m_chatterCooldown;
        m_searchCooldownOrigin = m_searchCooldown;

        //Generate Drop
        if(m_emitter == null)
        {
            //Select one of the two emitters
            int randomNum = Random.Range(0, 2);
            m_emitter = (randomNum == 0) ? Resources.Load<GameObject>("Prefabs/Scene/CreditEmitter") : Resources.Load<GameObject>("Prefabs/Scene/AmmoEmitter");
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (m_player.GetComponent<PlayerController>().m_lockControls) return;

        var direction = transform.position - new Vector3(m_player.transform.position.x, 1, m_player.transform.position.z);

        if(m_alerted && m_activeAI)
        {
            m_chatterCooldown -= Time.deltaTime;
            m_searchCooldown -= Time.deltaTime;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, -direction, out hit))
            {
                //if (m_agent.destination != transform.position)
                //{
                //    m_agent.destination = transform.position;
                //}

                if (hit.transform.tag == "Player")
                {
                    Debug.DrawRay(transform.position, -direction, Color.red);

                    //Begin Fire IEnumerator
                    if (!m_firing)
                    {
                        StartCoroutine(IFire(1.0f));
                    }
                }
                else
                {
                    m_agent.SetDestination(m_player.transform.position);
                }
            }

            //Play chatter SFX
            if (m_chatterCooldown < 0)
            {
                AudioSource.PlayClipAtPoint(m_shootTalk, transform.position);
                m_chatterCooldown = m_chatterCooldownOrigin;
            }

            transform.rotation = Quaternion.LookRotation(-direction);

            if(m_searchCooldown < 0)
            {
                m_alerted = false;
                m_agent.SetDestination(transform.position);
                m_searchCooldown = m_searchCooldownOrigin;
                AudioSource.PlayClipAtPoint(m_lostSound, transform.position);
                m_chatterCooldown = m_chatterCooldownOrigin;
            }

            if(m_justAlerted == true)
            {
                //Play alert sound
                AudioSource.PlayClipAtPoint(m_alertSound, transform.position);
                m_justAlerted = false;
            }
        }

        //If not alerted, perform alertness check.
        if(m_currentRoom == m_player.GetComponent<PlayerController>().m_currentRoom && !m_alerted && m_activeAI)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, -direction, out hit))
            {
                if (hit.transform.tag == "Player")
                {
                    m_alerted = true;
                }
            }
            Debug.DrawRay(transform.position, -direction, Color.cyan);            
        }

        if (m_currentRoom != m_player.GetComponent<PlayerController>().m_currentRoom && m_alerted && m_activeAI)
        {
            if(m_chatterCooldown < 0)
            {
                //Play hiding SFX
                AudioSource.PlayClipAtPoint(m_lostSound, transform.position);
                m_chatterCooldown = m_chatterCooldownOrigin;
            }            
        }

        if(m_health <= 0)
        {
            if(GetComponentInParent<Room>())
            {
                GetComponentInParent<Room>().m_inhabitants.Remove(this);
            }

            //Spawn drop from kill
            GameObject emitter = (GameObject)Instantiate(m_emitter, transform.position, transform.rotation);
            
            if(emitter.GetComponent<CreditEmitter>())
            {
                emitter.GetComponent<CreditEmitter>().Emit();
            }
            else if(emitter.GetComponent<AmmoEmitter>())
            {
                emitter.GetComponent<AmmoEmitter>().Emit();
            }

            //Spawn ragdoll on our location and rotate it accordingly
            Instantiate(m_ragdoll, new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), transform.rotation);

            Destroy(this.gameObject);
        }
	}

    /// <summary>
    /// Set this enemy to inhabit a room
    /// </summary>
    /// <param name="_room">Room to inhabit</param>
    public void SetRoom(Room _room)
    {
        m_currentRoom = _room;
    }
    
    /// <summary>
    /// Damage this enemy a set amount
    /// </summary>
    /// <param name="_damage">Damage dealt</param>
    public void Damage(float _damage)
    {
        if (!m_alerted) 
            m_alerted = true;

        m_health -= _damage;
    }

    public void OnTriggerEnter(Collider trigger)
    {
        if(trigger.gameObject.GetComponentInParent<Room>())
        {
            m_currentRoom = trigger.gameObject.GetComponentInParent<Room>();
        }
    }

    IEnumerator IFire(float _cooldown)
    {
        m_firing = true;
        GetComponentInChildren<WeaponBase>().Fire();
        yield return new WaitForSeconds(_cooldown);
        m_firing = false;
        yield return null;
    }

    //Reveal The Enemy
    private bool m_lerping = false;

    public void RevealEnemy()
    {
        StartCoroutine(ERevealEnemy());
    }

    public void HideEnemy(float _alphaMin)
    {
        StartCoroutine(EHideEnemy(_alphaMin));
    }

    IEnumerator ERevealEnemy()
    {
        m_lerping = true;

        Color color = GetComponentInChildren<SkinnedMeshRenderer>().material.color;

        for (float i = color.a; i < 1; )
        {
            i += Time.deltaTime;
            foreach(SkinnedMeshRenderer rend in GetComponentsInChildren<SkinnedMeshRenderer>())
                rend.material.color = new Color(color.r, color.g, color.b, i);
            yield return null;
        }

        m_lerping = false;
        yield return null;
    }

    IEnumerator EHideEnemy(float _alphaMin)
    {
        m_lerping = true;

        Color color = GetComponentInChildren<SkinnedMeshRenderer>().material.color;

        if (color.a < _alphaMin)
        {
            for (float i = color.a; i < _alphaMin; )
            {
                i += Time.deltaTime;
                foreach (SkinnedMeshRenderer rend in GetComponentsInChildren<SkinnedMeshRenderer>())
                    rend.material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }
        else
        {
            for (float i = color.a; i > _alphaMin; )
            {
                i -= Time.deltaTime;
                foreach (SkinnedMeshRenderer rend in GetComponentsInChildren<SkinnedMeshRenderer>())
                    rend.material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }

        m_lerping = false;
        yield return null;
    }
}
