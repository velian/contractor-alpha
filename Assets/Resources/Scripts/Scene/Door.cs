﻿using UnityEngine;
using System.Collections;

public class Door : Interactable
{
    public AudioClip m_audioClip;
    Animator m_animator;
    bool m_open = false;

    public override void ObjectUpdate()
    {
        if(m_triggered && !m_open)
        {
            m_open = true;

            //Play sound
            AudioSource.PlayClipAtPoint(m_audioClip, transform.position);

            //Perform animation
            m_animator = GetComponent<Animator>();
            if(DoubleDoors())
            {
                m_animator.Play("OpenDouble");   
            }
            else
            {
                m_animator.Play("OpenSingle");   
            }                     
        }
    }

    bool DoubleDoors()
    {
        int doorCount = 0;
        Transform[] m_transformList = GetComponentsInChildren<Transform>();

        foreach(Transform trans in m_transformList)
        {
            if(trans == this.transform)continue;

            if(trans.gameObject.activeInHierarchy)
            {
                doorCount++;
            }
        }

        if(doorCount > 2)
        {
            return true;
        }
        return false;
    }
}
