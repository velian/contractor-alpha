﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueTrigger : MonoBehaviour {

    public bool m_oneShotTrigger = true;
    public DialogueUI.DialogueSet m_dialogueSet;

    bool m_triggered = false;

    DialogueUI m_dialogueUI;

	// Use this for initialization
	void Start()
    {
        m_dialogueUI = GameObject.Find("DialogueUI").GetComponent<DialogueUI>();        
	}
	
    // On Trigger, show handed dialogue.
	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (m_oneShotTrigger && !m_triggered || !m_oneShotTrigger)
            {
                m_dialogueUI.Show();
                m_dialogueUI.PerformSequence(m_dialogueSet);
                m_triggered = true;
            }
        }
    }
}
