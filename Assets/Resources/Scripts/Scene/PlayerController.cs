﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public float m_speed = 20;
    public float m_speedMultiplier = 0.25f;

    public float m_shieldTotal = 2;

    public List<GameObject> m_weapons = new List<GameObject>();
    public WeaponBase.WEAPON_TYPE m_currentWeapon;

    public Room m_currentRoom = null;

    public bool m_recentlyShot = false;
    public bool m_lockControls = false;

    int m_weaponIndex = 0;
    float m_currentSpeed = 0;

    int m_loadedLevel;
    int m_shieldOrigin;    

    Vector3 m_forward;
    Vector3 m_right;
    Vector3 m_oldRotation;

    GameObject m_laserDot;
    Transform m_weaponTransform;

    ChromaticAberration m_aberration;

    void Start()
    {
        Initialize();       
    }

    void Initialize()
    {
        transform.position = Vector3.zero;
        m_loadedLevel = Application.loadedLevel;

        m_laserDot = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/LaserDot"));
        m_weaponTransform = GameObject.FindGameObjectWithTag("WeaponTransform").transform;
        m_aberration = GameObject.Find("Main Camera").GetComponent<ChromaticAberration>();

        m_shieldOrigin = 2;

        GetComponent<AudioSource>().Stop();

        //Set axis for controller movements
        m_forward = Camera.main.transform.forward;
        m_forward.y = 0;
        m_forward = Vector3.Normalize(m_forward);
        m_right = Quaternion.Euler(new Vector3(0, 90, 0)) * m_forward;

        foreach(GameObject weapon in m_weapons)
        {
            Destroy(weapon);
        }

        m_weapons.Clear();

        //Add a list of all the equipped weapons (which get childed during initialization)
        m_weapons.AddRange(PlayerInventory.m_manager.GetComponent<PlayerInventory>().GetEquipped());

        GetComponentsInChildren<WeaponBase>();
        SetWeapon(WeaponBase.WEAPON_TYPE.PISTOL);

        foreach (GameObject weapon in m_weapons)
        {
            weapon.GetComponent<WeaponBase>().ChildToPlayer();
        }
    }

	// Update is called once per frame
	void Update ()
    {
        if(m_loadedLevel != Application.loadedLevel)
        {
            //Init Script
            transform.position = Vector3.zero;
            Initialize();
        }

        if (!m_lockControls)
        {
            if (Input.GetJoystickNames().Length > 0)
            {
                HandleJoystick();
                HandleWeapon(true);
            }
            else
            {
                HandleKeyboard();
                HandleWeapon(false);
            }

            //Handle chromatic aberration here
            if (m_recentlyShot || m_aberration.ChromaticAbberation > 0)
            {
                if (!GetComponent<AudioSource>().isPlaying)
                {
                    GetComponent<AudioSource>().Play();
                    GetComponent<AudioSource>().volume = 0.05f;
                }

                m_aberration.ChromaticAbberation -= Time.deltaTime;

                StartCoroutine(IShotTimer());
            }
            else
            {
                if (m_shieldTotal < m_shieldOrigin)
                {
                    m_shieldTotal += Time.deltaTime;
                }

                GetComponent<AudioSource>().DOFade(0, 1).OnComplete(() =>
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().DOFade(0.05f, 0);
                });
            }
        }
	}

    void HandleKeyboard()
    {
        Plane groundPlane = new Plane(Vector3.up, 0);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        Vector3 hitPos = new Vector3(0, 0, 0);

        if (groundPlane.Raycast(ray, out distance))
        {
            hitPos = ray.GetPoint(distance);
            hitPos.y = transform.position.y;
            transform.LookAt(hitPos);
        }

        bool bMoving = false;

        //if(Input.GetKey(KeyCode.Space))
        //{
        //    //Stop all animations
        //    GetComponentInChildren<Animator>().SetBool("bMoving", bMoving);
        //
        //    //Play crouch anim
        //    
        //
        //    //Restrict firing on weapons
        //
        //
        //    return;
        //}

        Vector3 direction = new Vector3(0, 0, 0);

        m_currentSpeed = m_speed;
        
        if (Input.GetKey(KeyCode.W))
        {
            direction += Vector3.forward + Vector3.right;
            //bMoving = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction += Vector3.left + Vector3.forward;
            //bMoving = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction -= Vector3.forward - Vector3.left;
            //bMoving = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector3.right - Vector3.forward;
            //bMoving = true;
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            m_currentSpeed = m_speed * m_speedMultiplier;
        }

        if (direction.magnitude > 0.0f)
        {
            direction.Normalize();

            //float fForwardDot = Vector3.Dot(transform.forward, direction);
            //bool forward = fForwardDot < 0 ? false : true;

            GetComponent<Rigidbody>().AddForce(direction * Time.deltaTime * m_currentSpeed);
                        
            //if (!forward)
            //    fForwardDot = Mathf.Clamp01(-fForwardDot);
            //else
            //    fForwardDot = Mathf.Clamp01(fForwardDot);

            //GetComponentInChildren<Animator>().SetBool("bForward", forward);
            //GetComponentInChildren<Animator>().SetFloat("fMovingDot", fForwardDot);
        }

        //GetComponentInChildren<Animator>().SetBool("bMoving", bMoving);    
    }

    void HandleJoystick()
    {
        m_currentSpeed = m_speed;

        bool bMoving = false;
        Vector2 lStick = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        Vector2 rStick = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);

        //Handle right stick as rotational look
        if (rStick.magnitude > 0.2)
        {
            Vector3 lookDirection = new Vector3(rStick.x, 0, rStick.y);

            Vector3 rightLook = m_right * m_currentSpeed * Time.deltaTime * lookDirection.x;
            Vector3 upLook = m_forward * m_currentSpeed * Time.deltaTime * lookDirection.z;

            Vector3 lookDir = Vector3.Normalize(rightLook + upLook);
            
            //Lerp to look
            transform.forward = Vector3.Slerp(transform.forward, lookDir, Vector3.Distance(transform.forward, lookDir));
        }
        //else  //If no right stick, set to move direction (left stick)
        //{
        //    Vector3 rightMovement = m_right * m_currentSpeed * Time.deltaTime * lStick.x;
        //    Vector3 upMovement = m_forward * m_currentSpeed * Time.deltaTime * lStick.y;

        //    Vector3 lookDir = Vector3.Normalize(rightMovement + upMovement);

        //    if (transform.forward != lookDir && lStick.magnitude > 0.1f)
        //    {
        //        transform.forward = lookDir;
        //    }
        //}

        //Sprint
        if (GamePad.GetButton(GamePad.Button.LeftStick, GamePad.Index.One))
        {
            m_currentSpeed = m_speed * m_speedMultiplier;
        }

        //Crouch if key down, disallow input
        if (GamePad.GetButton(GamePad.Button.RightShoulder, GamePad.Index.One))
        {
            //Stop all anims
            if (GetComponentInChildren<Animator>().GetBool("bMoving") == true)
                bMoving = false;

            //Crouch animation
            if (GetComponentInChildren<Animator>().GetBool("bCrouching") == false)
                GetComponentInChildren<Animator>().SetBool("bCrouching", true);
        }
        else //If no crouch, allow movement
        {
            if(GetComponentInChildren<Animator>().GetBool("bCrouching") == true)
                GetComponentInChildren<Animator>().SetBool("bCrouching", false);

            //If moving
            if (lStick.magnitude > 0.1f)
            {
                Vector3 rightMovement = m_right * m_currentSpeed * Time.deltaTime * lStick.x;
                Vector3 upMovement = m_forward * m_currentSpeed * Time.deltaTime * lStick.y;
                Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

                GetComponent<Rigidbody>().AddForce(rightMovement + upMovement);

                bMoving = true;

                //Calculate direction and according animation
                float fForwardDot = Vector3.Dot(transform.forward, heading);

                GetComponentInChildren<Animator>().SetFloat("fDirection", fForwardDot);
                GetComponentInChildren<Animator>().SetFloat("fStrafeDirection", (rStick.y > 0) ? lStick.x : -lStick.x);
                GetComponentInChildren<Animator>().SetFloat("fRunSpeed", lStick.magnitude);

                //GetComponentInChildren<Animator>().SetBool("bForward", forward);    

                //float fForwardDot = Vector3.Dot(transform.forward, direction);
                //bool forward = fForwardDot < 0 ? false : true;

                //GetComponent<Rigidbody>().AddForce(new Vector3(direction.x, 0, direction.y) * Time.deltaTime * m_currentSpeed);

                //if (!forward)
                //    fForwardDot = Mathf.Clamp01(-fForwardDot);
                //else
                //fForwardDot = Mathf.Clamp01(fForwardDot);

                //GetComponentInChildren<Animator>().SetBool("bForward", forward);
                //GetComponentInChildren<Animator>().SetFloat("fMovingDot", fForwardDot);
            }
        }        

        GetComponentInChildren<Animator>().SetBool("bMoving", bMoving);
    }

    void HandleWeapon(bool _joystick)
    {
        //Render laser dot
        RaycastHit hitInfo;
        if (Physics.Raycast(m_weaponTransform.position, -m_weaponTransform.right, out hitInfo))
        {
            m_laserDot.transform.position = hitInfo.point;
            Quaternion rot = Quaternion.FromToRotation(-Vector3.forward, hitInfo.normal);
            m_laserDot.transform.rotation = rot;
        }
        else
        {
            m_laserDot.transform.position = Vector3.zero;
            m_laserDot.transform.rotation = Quaternion.identity;
        }

        if (_joystick == false) //If we have no joystick, use the keyboard keys
        {
            //Get key for weapon
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                //Set weapon pistol
                SetWeapon(WeaponBase.WEAPON_TYPE.PISTOL);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                //Set weapon carbine
                SetWeapon(WeaponBase.WEAPON_TYPE.CARBINE);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                //Set weapon shotgun
                SetWeapon(WeaponBase.WEAPON_TYPE.SHOTGUN);
            }
        }
        else
        {
            if (GamePad.GetButtonDown(GamePad.Button.Y, GamePad.Index.One))
            {
                switch (m_weaponIndex)
                {
                    case 0:
                        {
                            SetWeapon(WeaponBase.WEAPON_TYPE.CARBINE);
                        }
                        break;
                    case 1:
                        {
                            SetWeapon(WeaponBase.WEAPON_TYPE.SHOTGUN);
                        }
                        break;
                    case 2:
                        {
                            SetWeapon(WeaponBase.WEAPON_TYPE.PISTOL);
                            m_weaponIndex = -1;
                        }
                        break;
                }
                m_weaponIndex++;
            }
        }
    }

    /// <summary>
    /// Set weapon for player from equipped weapons
    /// </summary>
    /// <param name="_type">Wapon to set to active</param>
    void SetWeapon(WeaponBase.WEAPON_TYPE _type)
    {
        foreach(GameObject weapon in m_weapons)
        {
            if (weapon.GetComponent<WeaponBase>().m_weaponType != _type)
            {
                if (weapon.GetComponent<WeaponBase>().m_ui != null)
                    weapon.GetComponent<WeaponBase>().m_ui.gameObject.SetActive(false);
                weapon.SetActive(false);
            }
            else
            {
                if (weapon.GetComponent<WeaponBase>().m_ui != null)
                    weapon.GetComponent<WeaponBase>().m_ui.gameObject.SetActive(true);
                weapon.SetActive(true);                
            }
        }
    }

    public void SetRoom(Room _room)
    {
        m_currentRoom = _room;
    }

    public void Damage(int _damage)
    {
        m_shieldTotal -= _damage;

        if (m_shieldTotal < 0)
        {
            //Initialize();
            Kill();
            //GetComponent<AudioSource>().Stop();
            return;
        }

        StartCoroutine(IChromaticAberration(0.25f, _damage * 1.5f));
    }

    public void Kill()
    {
        //Fade to black, reload level.
        Application.LoadLevel(Application.loadedLevel);
        m_aberration.ChromaticAbberation = 0;
        DestroyImmediate(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Bullet>())
        {
            if(collision.gameObject.GetComponent<Bullet>().m_owner == name) return;

            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());

            Destroy(collision.gameObject);
        }
    }

    IEnumerator IChromaticAberration(float _duration, float _amount)
    {
        for (float i = 0; i < _duration; i += Time.deltaTime)
        {
            m_aberration.ChromaticAbberation += i * _amount;

            yield return null;
        }

        yield return null;
    }

    IEnumerator IShotTimer()
    {
        if (!m_recentlyShot)
            yield return null;

        yield return new WaitForSeconds(1.0f);
        m_recentlyShot = false;
        yield return null;
    }
}
