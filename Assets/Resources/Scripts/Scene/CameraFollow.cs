﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    [Header("Settings")]
    public float m_smoothness = 1;

    Transform m_player;
    Vector3 m_velocity = Vector3.zero;

	// Use this for initialization
	void Start()
    {
        m_player = GameObject.Find("Player").transform;
	}
	
	// Update is called once per frame
	void Update()
    {
        Vector3 point = Camera.main.WorldToViewportPoint(m_player.position);
        Vector3 delta = m_player.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, new Vector3(destination.x, 16, destination.z), ref m_velocity, m_smoothness);
	}
}
