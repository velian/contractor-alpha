﻿using UnityEngine;
using System.Collections;


public class TileRenderer : MonoBehaviour {

    public Sprite[] m_spriteList;

	// Use this for initialization
	void Start()
    {
	    foreach(Sprite sprite in m_spriteList)
        {
            GameObject gameObject = new GameObject();
            gameObject.transform.position = this.transform.position;
            gameObject.transform.rotation = this.transform.rotation;
            gameObject.AddComponent<SpriteRenderer>();
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
            gameObject.transform.parent = this.transform;
        }

        //Todo in da future, m89
        //Combine meshes in children to this background
	}
}
