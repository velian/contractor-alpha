﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour {

    public static GameObject m_manager;
    public GameObject[] m_weaponPrefabList;
    public List<GameObject> m_weaponGameObjects = new List<GameObject>();
    public List<WeaponConfiguration> m_weaponList = new List<WeaponConfiguration>();   

    public int m_credits;

    // Use this for initialization
    void Awake()
    {
        if (m_manager == null)
        {
            DontDestroyOnLoad(gameObject);
            m_manager = gameObject;
            Initialize();
        }
        else if (m_manager != gameObject)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// When the application quits, save vals
    /// </summary>
    void OnApplicationQuit()
    {
        SetWeaponList();
    }

    /// <summary>
    /// Manages the weapon list and the like
    /// </summary>
    void Update()
    {
        //Debug key overrides
        if (Input.GetKey(KeyCode.O))
            m_credits += 1000;

        if (Input.GetKeyDown(KeyCode.P))
        {
            print("SAVE FILE RESET!");
            InitialSaveFile();
        }
        //        
    }

    /// <summary>
    /// Calls get weapon list on initialize
    /// </summary>
    void Initialize()
    {
        //PlayerPrefs.DeleteAll();

        if (HaveSaveFile())
            GetWeaponList();
        else
            InitialSaveFile();
    }

    /// <summary>
    /// Reset the save file to contain a list of base weapons
    /// </summary>
    void InitialSaveFile()
    {
        //Delete previous weapons
        m_weaponList.Clear();

        foreach(GameObject weapon in m_weaponPrefabList)
        {
            GameObject newWeapon = Instantiate(weapon);
            newWeapon.GetComponent<WeaponBase>().Initialize(newWeapon.GetComponent<WeaponBase>());
            newWeapon.GetComponent<WeaponBase>().ChildToPlayer();
            //newWeapon.name = "Weapon";

            m_weaponGameObjects.Add(newWeapon);

            WeaponConfiguration weaponConfig = new WeaponConfiguration();
            weaponConfig.Initialize(newWeapon.GetComponent<WeaponBase>().m_weaponName, newWeapon.GetComponent<WeaponBase>().m_weaponType, new WeaponBase.Attributes(1, 1));

            weaponConfig.weaponCost = newWeapon.GetComponent<WeaponBase>().m_weaponBaseValue;
            weaponConfig.weaponEquipped = true;

            m_weaponList.Add(weaponConfig);

            //SetCurrentWeapon(newWeapon.GetComponent<WeaponBase>());
        }

        SetWeaponList();
    }

    /// <summary>
    /// Sets the list of weapons in the player prefs to our list of weapons in the PlayerInventory
    /// </summary>
    public void SetWeaponList()
    {
        PlayerPrefs.DeleteAll();

        PlayerPrefs.SetInt("WeaponListSize", m_weaponList.Count);
        PlayerPrefs.SetInt("Credits", m_credits);

        for (int i = 0; i < m_weaponList.Count; i++)
        {
            PlayerPrefs.SetString("WeaponName"  + i, (string)m_weaponList[i].weaponName);
            PlayerPrefs.SetInt("WeaponType"     + i,    (int)m_weaponList[i].weaponType);
            PlayerPrefs.SetInt("WeaponDamage"   + i,    (int)m_weaponList[i].weaponAttributes.m_damage);
            PlayerPrefs.SetInt("WeaponShield"   + i,    (int)m_weaponList[i].weaponAttributes.m_shield);
            PlayerPrefs.SetInt("WeaponCost"     + i,    (int)m_weaponList[i].weaponCost);
            PlayerPrefs.SetInt("WeaponEquipped" + i,        (m_weaponList[i].weaponEquipped == true) ? 1 : 0);
        }

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Get the list of weapons from the playerprefs and store it in the m_weapons variable
    /// </summary>
    public void GetWeaponList()
    {
        if (PlayerPrefs.GetInt("WeaponListSize") == 0) return;

        m_credits = PlayerPrefs.GetInt("Credits");

        for (int i = 0; i < PlayerPrefs.GetInt("WeaponListSize"); i++)
        {
            WeaponConfiguration wepConfig = new WeaponConfiguration();
            wepConfig.Initialize(
                (string)(PlayerPrefs.GetString("WeaponName" + i)),
                (WeaponBase.WEAPON_TYPE)(PlayerPrefs.GetInt("WeaponType" + i)),
                new WeaponBase.Attributes((PlayerPrefs.GetInt("WeaponDamage" + i)), (PlayerPrefs.GetInt("WeaponShield" + i))),
                (int)(PlayerPrefs.GetInt("WeaponCost" + i)));

            //wepConfig.weaponName =                  (string)(PlayerPrefs.GetString("WeaponName" + i));
            //wepConfig.weaponType =                  (WeaponBase.WEAPON_TYPE)(PlayerPrefs.GetInt("WeaponType" + i));            
            //wepConfig.weaponAttributes.m_damage =   (uint)(PlayerPrefs.GetInt("WeaponDamage" + i));
            //wepConfig.weaponAttributes.m_shield =   (uint)(PlayerPrefs.GetInt("WeaponShield" + i));
            //wepConfig.weaponCost =                  (int)(PlayerPrefs.GetInt("WeaponCost" + i));
            wepConfig.weaponEquipped =              ((PlayerPrefs.GetInt("WeaponEquipped" + i) == 1) ? true : false);

            m_weaponList.Add(wepConfig);
        }
    }

    /// <summary>
    /// Get a list of equipped weapons from the configs as gameobjects
    /// </summary>
    /// <returns>GameObject[]; list of equipped as gameobjects</returns>
    public GameObject[] GetEquipped()
    {
        GameObject[] weapons = new GameObject[3];

        //Return equipped weapon configs as gameobjects initialized
        foreach(WeaponConfiguration config in m_weaponList)
        {
            if (!config.weaponEquipped) continue;

            GameObject weapon = new GameObject();
            //weapon.GetComponent<WeaponBase>().Initialize();

            if(config.weaponType == WeaponBase.WEAPON_TYPE.PISTOL)
            {
                //Add object
                weapon.AddComponent<Revolver>();
                weapon.GetComponent<Revolver>().m_weaponType = config.weaponType;
                weapon.GetComponent<Revolver>().Initialize(m_weaponPrefabList[0].GetComponent<WeaponBase>());
                weapon.name = "Revolver";
                weapons[0] = weapon;
            }
            else if (config.weaponType == WeaponBase.WEAPON_TYPE.CARBINE)
            {
                //Add object
                weapon.AddComponent<Carbine>();
                weapon.GetComponent<Carbine>().m_weaponType = config.weaponType;
                weapon.GetComponent<Carbine>().Initialize(m_weaponPrefabList[1].GetComponent<WeaponBase>());
                weapon.name = "Carbine";
                weapons[1] = weapon;
            }
            else if (config.weaponType == WeaponBase.WEAPON_TYPE.SHOTGUN)
            {
                //Add object
                weapon.AddComponent<Shotgun>();
                weapon.GetComponent<Shotgun>().m_weaponType = config.weaponType;
                weapon.GetComponent<Shotgun>().Initialize(m_weaponPrefabList[2].GetComponent<WeaponBase>());
                weapon.name = "Shotgun";
                weapons[2] = weapon;
            }
        }

        return weapons;
    }

    bool HaveSaveFile()
    {
        if (!PlayerPrefs.HasKey("WeaponListSize") || PlayerPrefs.GetInt("WeaponListSize") == 0)
            return false;
        else
            return true;
    }    
}